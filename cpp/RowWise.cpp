// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>

#include "./parser.h"


int main() {
   std::string INPUTFILE = "../your_data_dir/test_public_v2.csv";
   std::string OUTPUTFILE = "test_public_v2_woFOI.csv";
   std::string OUTPUTFILE_FOI = "test_public_v2_FOI.csv";
   const std::string headers1[N_RAW_FEATURES] = {"ncl[0]","ncl[1]","ncl[2]","ncl[3]",
      "avg_cs[0]","avg_cs[1]","avg_cs[2]","avg_cs[3]","ndof",
      "MatchedHit_TYPE[0]","MatchedHit_TYPE[1]","MatchedHit_TYPE[2]","MatchedHit_TYPE[3]",
      "MatchedHit_X[0]","MatchedHit_X[1]","MatchedHit_X[2]","MatchedHit_X[3]",
      "MatchedHit_Y[0]","MatchedHit_Y[1]","MatchedHit_Y[2]","MatchedHit_Y[3]",
      "MatchedHit_Z[0]","MatchedHit_Z[1]","MatchedHit_Z[2]","MatchedHit_Z[3]",
      "MatchedHit_DX[0]","MatchedHit_DX[1]","MatchedHit_DX[2]","MatchedHit_DX[3]",
      "MatchedHit_DY[0]","MatchedHit_DY[1]","MatchedHit_DY[2]","MatchedHit_DY[3]",
      "MatchedHit_DZ[0]","MatchedHit_DZ[1]","MatchedHit_DZ[2]","MatchedHit_DZ[3]",
      "MatchedHit_T[0]","MatchedHit_T[1]","MatchedHit_T[2]","MatchedHit_T[3]",
      "MatchedHit_DT[0]","MatchedHit_DT[1]","MatchedHit_DT[2]","MatchedHit_DT[3]",
      "Lextra_X[0]","Lextra_X[1]","Lextra_X[2]","Lextra_X[3]",
      "Lextra_Y[0]","Lextra_Y[1]","Lextra_Y[2]","Lextra_Y[3]","NShared",
      "Mextra_DX2[0]","Mextra_DX2[1]","Mextra_DX2[2]","Mextra_DX2[3]",
      "Mextra_DY2[0]","Mextra_DY2[1]","Mextra_DY2[2]","Mextra_DY2[3]","FOI_hits_N","PT","P"};
   //additional features in the input file
   //const size_t N_ADD_FEATURES = 3;
   //const std::string headers_add[N_ADD_FEATURES] = {"particle_type","label","weight"};
   const size_t N_ADD_FEATURES = 0;
   const std::string headers_add[N_ADD_FEATURES] = {};
   //features calculated in this program
   const size_t N_MY_FEATURES = 2*4;
   const std::string headers2[N_MY_FEATURES] = {"sigma_x_0","sigma_x_1", "sigma_x_2", "sigma_x_3",
					  "sigma_y_0","sigma_y_1", "sigma_y_2", "sigma_y_3"};
   std::ifstream fin;
   fin.open(INPUTFILE);
   std::ofstream fout1;
   fout1.open(OUTPUTFILE);
   std::ofstream fout2;
   fout2.open(OUTPUTFILE_FOI);
   std::string line;
   bool header = true;
   fout1 << "id" << DELIMITER;
   for(size_t i=0; i<N_RAW_FEATURES-1; ++i)
      fout1 << headers1[i] << DELIMITER;
   fout1 << headers1[N_RAW_FEATURES-1];
   for(size_t i=0; i<(N_ADD_FEATURES>0?N_ADD_FEATURES-1:0); ++i){
      if(i==0) fout1 << DELIMITER;
      fout1 << headers_add[i]  << DELIMITER;
   }
   if(N_ADD_FEATURES>0) fout1 << headers_add[N_ADD_FEATURES-1];
   fout1 << '\n';
   fout2 << "id" << DELIMITER;
   for(size_t i=0; i<N_MY_FEATURES-1; ++i)
      fout2 << headers2[i]  << DELIMITER;
   fout2 << headers2[N_MY_FEATURES-1]  << '\n';

   while(std::getline(fin, line)){
      if(header) {
	 header = false;
	 continue;
      }
      std::vector<float> features(N_RAW_FEATURES+N_ADD_FEATURES+N_MY_FEATURES);
      size_t id;
      std::istringstream stream1(line);
      std::string input1; 
      ugly_hardcoded_parse(stream1, &id, &features, N_ADD_FEATURES);

      fout1 << id << DELIMITER;
      for(size_t i=0; i<N_RAW_FEATURES-1; ++i)
	 fout1 << features[i] << DELIMITER;
      fout1 << features[N_RAW_FEATURES-1];
      for(size_t i=0; i<(N_ADD_FEATURES>0?N_ADD_FEATURES-1:0); ++i){
	 if(i==0) fout1 << DELIMITER;
	 fout1 << features[N_RAW_FEATURES+i] << DELIMITER;
      }
      if(N_ADD_FEATURES>0) fout1 << features[N_RAW_FEATURES+N_ADD_FEATURES-1];
      fout1 << '\n';

      fout2 << id << DELIMITER;
      for(size_t i=0; i<N_MY_FEATURES-1; ++i)
	 fout2 << features[N_RAW_FEATURES+N_ADD_FEATURES+i] << DELIMITER;
      fout2 << features[N_RAW_FEATURES+N_ADD_FEATURES+N_MY_FEATURES-1] << '\n';
      
   }
   return 0;
}
