import sys
from itertools import repeat
import numpy as np
import pandas as pd
import time
import calc_var as cv

SIMPLE_FEATURE_COLUMNS = ['MatchedHit_TYPE[0]',
       'MatchedHit_TYPE[1]', 'MatchedHit_TYPE[2]', 'MatchedHit_TYPE[3]',
       'MatchedHit_X[0]', 'MatchedHit_X[1]', 'MatchedHit_X[2]',
       'MatchedHit_X[3]', 'MatchedHit_Y[0]', 'MatchedHit_Y[1]',
       'MatchedHit_Y[2]', 'MatchedHit_Y[3]', 'MatchedHit_Z[0]',
       'MatchedHit_Z[1]', 'MatchedHit_Z[2]', 'MatchedHit_Z[3]',
       'MatchedHit_DX[0]', 'MatchedHit_DX[1]', 'MatchedHit_DX[2]',
       'MatchedHit_DX[3]', 'MatchedHit_DY[0]', 'MatchedHit_DY[1]',
       'MatchedHit_DY[2]', 'MatchedHit_DY[3]', 'MatchedHit_DZ[0]',
       'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]',
       'Lextra_X[0]', 'Lextra_X[1]',
       'Lextra_X[2]', 'Lextra_X[3]', 'Lextra_Y[0]', 'Lextra_Y[1]',
       'Lextra_Y[2]', 'Lextra_Y[3]', 'Mextra_DX2[0]',
       'Mextra_DX2[1]', 'Mextra_DX2[2]', 'Mextra_DX2[3]', 'Mextra_DY2[0]',
       'Mextra_DY2[1]', 'Mextra_DY2[2]', 'Mextra_DY2[3]']
EXTRA_FEATURE_COLUMNS = []
ID_COLUMN = "id"


def main():
    #read data
    types = dict(zip(SIMPLE_FEATURE_COLUMNS, repeat(np.float32)))
    types[ID_COLUMN] = np.uint64
    data = pd.read_csv('../../idao/your_data_dir/train_part_2_v2.csv', usecols=[ID_COLUMN] + SIMPLE_FEATURE_COLUMNS + EXTRA_FEATURE_COLUMNS,
                       dtype=types, index_col=ID_COLUMN, chunksize=15000)
    types.update(dict(zip(EXTRA_FEATURE_COLUMNS, repeat(np.float32))))
    #predict
    sys.stdout.write("id,prediction\n")
    n = 0
    for chunk in data:
        print('processing ' + str(n) + ' - ' + str(n+len(chunk)))
        predictions = 1./(cv.MeasCalculator()).calc_AverageSquaredDistance3(chunk)['AverageSquaredDistance3']
        #predictions = (cv.MeasCalculator()).calc_AverageSquaredDistance3(chunk)['InvASD3']
        if n==0 :
            pd.DataFrame(data={"prediction": predictions}, index=chunk.index).to_csv(
                'submmision_train2_ASD3inv.csv', index_label=ID_COLUMN, header=False, mode='w')
        else:
            pd.DataFrame(data={"prediction": predictions}, index=chunk.index).to_csv(
                'submmision_train2_ASD3inv.csv', index_label=ID_COLUMN, header=False, mode='a')
        n += len(chunk)


if __name__ == "__main__":
    sttime = time.time()
    main()
    entime = time.time()
    print('calculation time : ' + str(entime - sttime) + '(s)')

