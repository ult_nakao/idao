#!/bin/bash

CONDA_DIR="/anaconda"
source $CONDA_DIR/etc/profile.d/conda.sh
conda activate

python ./ASD3inv.py
