from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

module_name = 'calc_var'
setup(
        name = module_name + ' app',
        cmdclass = {'build_ext':build_ext},
        ext_modules = [Extension(module_name, [module_name + '.pyx'])]
    )
