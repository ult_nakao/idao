"""
Usage:
import calc_var as cv
event = (cv.MeasCalculator()).calc_*(event)
"""

import numpy as np
import pandas as pd

class MeasCalculator:
    def __init__(self):
        return

    def calc_all(self, events):
        events = self.calc_PCat(events)
        events = self.calc_PRatio(events)
        events = self.calc_DiffMatchedLextra(events)
        events = self.calc_AverageSquaredDistance3(events)
        return events

    def calc_PCat(self, events):

        mask = events['P'] < 3000
        mask = mask + 0
        thres = [6000, 8000, 10000, 20000, 30000, 40000, 50000, 60000] 
        for thre in thres:
            mask_tmp = events['P'] > thre
            mask_tmp = mask_tmp + 0
            mask = mask + mask_tmp

        tmp = pd.DataFrame(mask)
        events['PCat'] = tmp['P']
        return events
    
    def calc_PRatio(self, events):

        events['PRatio'] = events['PT'] / events['P']
        return events

    def calc_DiffMatchedLextra(self, events):
        nameMatched = "MatchedHit_"
        nameLextra  = "Lextra_"
        nameDiff    = "DiffMatchedLextra_"
        for xy in ['X', 'Y']:
            for station in [0, 1, 2, 3]:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDiff    = nameDiff     + xy + '[' + str(station) + ']'
                events[aNameDiff] = events[aNameMatched] - events[aNameLextra]
        
        return events

    def calc_AverageSquaredDistance(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/events[aNameDelta])**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_AverageSquaredDistance2(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance2" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**2 + events[aNameDeltaEx])**0.5)**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD2"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_AverageSquaredDistance3(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance3" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**0.5*events[aNameDeltaEx]**0.25))**2

        events[nameASD] /= events[nameNhit]

        #nameInvASD    = "InvASD3"
        #events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_AverageSquaredDistance4(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance4" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**(-1) + events[aNameDeltaEx]**(-0.5))**(-1))**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD4"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_IsMuon(self, events):
        nameMuon = 'IsMuon'
        events[nameMuon] = 0

        mask = (3000.0 <= events['P'])
        mask = mask & (events['P'] < 6000.0)
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        events.loc[mask, nameMuon] = 1

        mask = (6000.0 <= events['P'])
        mask = mask & (events['P'] < 10000.0)
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        mask = mask & ((events['MatchedHit_TYPE[2]'] > 0) | (events['MatchedHit_TYPE[3]'] > 0))
        events.loc[mask, nameMuon] = 1

        mask = (10000.0 <= events['P'])
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        mask = mask & ((events['MatchedHit_TYPE[2]'] > 0) & (events['MatchedHit_TYPE[3]'] > 0))
        events.loc[mask, nameMuon] = 1

        return events

