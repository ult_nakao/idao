import numpy as np
import tensorflow as tf

def weight_variable(shape, name, stddev = 0.1):
  initial = tf.truncated_normal(shape, stddev)
  return tf.Variable(initial, name)
 
def bias_variable(shape, name, stddev = 0.1):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial, name)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
 
def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def avg_pool_2x2(x):
  return tf.nn.avg_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def avg_pool_2x2_wo_strides(x):
  return tf.nn.avg_pool(x, ksize=[1, 2, 2, 1], strides=[1, 1, 1, 1], padding='SAME')
