import sys
sys.path.append("../../../yandex/")
import os
import h5py
import pandas as pd
import gc

import utils
import scoring
import calc_var as cv

#import model_def
import numpy as np
import tensorflow as tf
import model_def

from datetime import datetime

boolweightloss = True
booladam = False
boolhalfmuon = False

modeldir= "trained_pars/"

dir_dataname = "../../../your_data_dir/"

train_data, test_data = utils.load_data_csv(dir_dataname, utils.SIMPLE_FEATURE_COLUMNS)

train_data = (cv.MeasCalculator()).calc_all(train_data)
train_data = (cv.MeasCalculator()).calc_newfeature(train_data)
train_data = (cv.MeasCalculator()).calc_newfeature_sq(train_data)
train_data = (cv.MeasCalculator()).calc_newfeature_v2(train_data)

test_data = (cv.MeasCalculator()).calc_all(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature_sq(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature_v2(test_data)

train_data['dens_FOI_N0'] = (train_data['FOI_N0']/train_data['Mextra_DX2[0]']**0.5/train_data['Mextra_DY2[0]']**0.5)**0.25*4
train_data['dens_FOI_N1'] = (train_data['FOI_N1']/train_data['Mextra_DX2[1]']**0.5/train_data['Mextra_DY2[1]']**0.5)**0.25*4
train_data['dens_FOI_N2'] = (train_data['FOI_N2']/train_data['Mextra_DX2[2]']**0.5/train_data['Mextra_DY2[2]']**0.5)**0.25*4
train_data['dens_FOI_N3'] = (train_data['FOI_N3']/train_data['Mextra_DX2[3]']**0.5/train_data['Mextra_DY2[3]']**0.5)**0.25*4

test_data['dens_FOI_N0'] = (test_data['FOI_N0']/test_data['Mextra_DX2[0]']**0.5/test_data['Mextra_DY2[0]']**0.5)**0.25*4
test_data['dens_FOI_N1'] = (test_data['FOI_N1']/test_data['Mextra_DX2[1]']**0.5/test_data['Mextra_DY2[1]']**0.5)**0.25*4
test_data['dens_FOI_N2'] = (test_data['FOI_N2']/test_data['Mextra_DX2[2]']**0.5/test_data['Mextra_DY2[2]']**0.5)**0.25*4
test_data['dens_FOI_N3'] = (test_data['FOI_N3']/test_data['Mextra_DX2[3]']**0.5/test_data['Mextra_DY2[3]']**0.5)**0.25*4

#alldata 5445705
train_data_split = train_data[:5000000]
check_data_split = train_data[5000001:]

batch_num =  1024
n_update =  5000
try_num =  10001

train_ratio_muon = 0.5

ndim_data = 79 + 8

def make_arranged_data(data, ratio_muon = 0.5):
    pion = data[data['particle_type']==0]
    muon = data[data['particle_type']==1]
    proton = data[data['particle_type']==2]

    n_data_nomuon = len(pion) + len(proton)

    muon_undersample = muon.sample(int(n_data_nomuon*ratio_muon/(1-ratio_muon)))

    newdata = pd.concat([pion, muon_undersample, proton])

    del pion
    del muon
    del proton
    del muon_undersample
    gc.collect()

    return newdata

def get_batch_data(data, _batch_num):
    sample = data.sample(_batch_num)
    
    match_r = sample[["MatchedHit_R_0" ,"MatchedHit_R_1" ,"MatchedHit_R_2" ,"MatchedHit_R_3"]]/5000.0
    region = sample[['region_0', 'region_1', 'region_2', 'region_3']]/4.0

    station0 = sample[['sq_chi_physX0', 'sq_chi_modelX0', 'sq_chi_physY0', 'sq_chi_modelY0']].values
    station1 = sample[['sq_chi_physX1', 'sq_chi_modelX1', 'sq_chi_physY1', 'sq_chi_modelY1']].values
    station2 = sample[['sq_chi_physX2', 'sq_chi_modelX2', 'sq_chi_physY2', 'sq_chi_modelY2']].values
    station3 = sample[['sq_chi_physX3', 'sq_chi_modelX3', 'sq_chi_physY3', 'sq_chi_modelY3']].values
    
    asd3 = sample[['AverageSquaredDistance3']].values
    chi_root_X_phys = sample[['chi_root_physX0']].values + sample[['chi_root_physX1']].values + sample[['chi_root_physX2']].values + sample[['chi_root_physX3']].values
    chi_root_Y_phys = sample[['chi_root_physY0']].values + sample[['chi_root_physY1']].values + sample[['chi_root_physY2']].values + sample[['chi_root_physY3']].values
    chi_root_X_model = sample[['chi_root_modelX0']].values + sample[['chi_root_modelX1']].values + sample[['chi_root_modelX2']].values + sample[['chi_root_modelX3']].values
    chi_root_Y_model = sample[['chi_root_modelY0']].values + sample[['chi_root_modelY1']].values + sample[['chi_root_modelY2']].values + sample[['chi_root_modelY3']].values
#21

    hitType = sample[['MatchedHit_TYPE[0]', 'MatchedHit_TYPE[1]', 'MatchedHit_TYPE[2]', 'MatchedHit_TYPE[3]']].values
    measX = sample[['MatchedHit_X[0]', 'MatchedHit_X[1]', 'MatchedHit_X[2]', 'MatchedHit_X[3]']].values/2000
    measY = sample[['MatchedHit_Y[0]', 'MatchedHit_Y[1]', 'MatchedHit_Y[2]', 'MatchedHit_Y[3]']].values/2000
    measZ0 = ((sample[['MatchedHit_Z[0]']]-15250)/150).values
    measZ1 = ((sample[['MatchedHit_Z[1]']]-16450)/150).values
    measZ2 = ((sample[['MatchedHit_Z[2]']]-17650)/150).values
    measZ3 = ((sample[['MatchedHit_Z[3]']]-18850)/150).values
    measZ = np.concatenate((measZ0, measZ1, measZ2, measZ3), axis=1)
    measT = sample[['MatchedHit_T[0]', 'MatchedHit_T[1]', 'MatchedHit_T[2]', 'MatchedHit_T[3]']].values/5
#20

    del_measX = 10.0/sample[['MatchedHit_DX[0]', 'MatchedHit_DX[1]', 'MatchedHit_DX[2]', 'MatchedHit_DX[3]']].values
    del_measY = 10.0/sample[['MatchedHit_DY[0]', 'MatchedHit_DY[1]', 'MatchedHit_DY[2]', 'MatchedHit_DY[3]']].values
    del_measZ = 10.0/sample[['MatchedHit_DZ[0]', 'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]']].values
    del_measT = sample[['MatchedHit_DT[0]', 'MatchedHit_DT[1]', 'MatchedHit_DT[2]', 'MatchedHit_DT[3]']].values/5
    del_modelX = 10.0/(sample[['Mextra_DX2[0]', 'Mextra_DX2[1]', 'Mextra_DX2[2]', 'Mextra_DX2[3]']].values)**0.5
    del_modelY = 10.0/(sample[['Mextra_DY2[0]', 'Mextra_DY2[1]', 'Mextra_DY2[2]', 'Mextra_DY2[3]']].values)**0.5
#24

    PT = sample[['sq_PT']].values
    P = sample[['sq_P']].values
    PRatio = sample[['PRatio']].values
    PCat = sample[['PCat']].values/8.0
    FOI_N0 = (sample[['FOI_N0']].values - 2)/3.0
    FOI_N1 = (sample[['FOI_N1']].values - 2)/3.0
    FOI_N2 = (sample[['FOI_N2']].values - 2)/3.0
    FOI_N3 = (sample[['FOI_N3']].values - 2)/3.0
    NShared = sample[['NShared']].values/7.0
    ndof = (sample[['ndof']].values - 4)/4.0
#10
    dens_FOI = sample[['dens_FOI_N0', 'dens_FOI_N1', 'dens_FOI_N2', 'dens_FOI_N3']].values
#4

    inputdata = np.concatenate((match_r ,region ,station0 ,station1 ,station2 ,station3 ,asd3 ,chi_root_X_phys ,chi_root_Y_phys ,chi_root_X_model ,chi_root_Y_model ,hitType ,measX ,measY ,measZ ,measT ,del_measX ,del_measY ,del_measZ ,del_measT ,del_modelX ,del_modelY ,PT ,P ,PRatio ,PCat ,FOI_N0 ,FOI_N1 ,FOI_N2 ,FOI_N3 ,NShared ,ndof ,dens_FOI) , axis=1)

    particle_type = np.identity(3)[sample['particle_type'].values]
    weight = sample[['weight']].values
    label = sample[['label']].values

    return inputdata, particle_type, weight, label

def train():
    outfilename = "out_"+datetime.now().strftime("%Y_%m_%d_%H_%M_%S")+".txt"
    outfiletxt = open(outfilename, "w")

#input
    keep_prob = tf.placeholder(tf.float32)
    is_training = tf.placeholder(dtype=bool)
    inputdata = tf.placeholder(tf.float32, [None, ndim_data])

    mean = tf.placeholder(tf.float32) # for loss function
#output
    particle_type = tf.placeholder(tf.float32, [None, 1])
    weight = tf.placeholder(tf.float32, [None, 1])

#estimator
    particle_type_predict = model_def.estimator(inputdata, keep_prob, is_training) 
    loss_function = model_def.loss_function(particle_type_predict, particle_type, weight, mean)
    loss_function_wo_weight = model_def.loss_function_wo_weight(particle_type_predict, particle_type, weight)
    trainer = 0
    if booladam == True and boolweightloss == True:
        trainer = model_def.trainer_adam(loss_function)
    if booladam == True and boolweightloss == False:
        trainer = model_def.trainer_adam(loss_function_wo_weight)
    if booladam == False and boolweightloss == True:
        trainer = model_def.trainer_rms(loss_function)
    if booladam == False and boolweightloss == False:
        trainer = model_def.trainer_rms(loss_function_wo_weight)

    config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver(max_to_keep=None)
    saver.restore(sess, "../model_256x256x256_twocategory_w_rawdata_w_weightlossfunc_w_RMSpropGraves_w_rootXY_onedropout_newpars/trained_pars/model_200000.ckpt")

    _test_inputdata, _test_particle_type, _test_weight, _test_label = get_batch_data(check_data_split, len(check_data_split))
    
    this_train_data = train_data_split
    if boolhalfmuon == True:
        this_train_data = make_arranged_data(train_data_split, train_ratio_muon)

    for i in range(try_num):
        _inputdata, _particle_type, _weight, _label = get_batch_data(this_train_data, batch_num)
        _mean = -25 + 50*np.exp(-i/5000.0)
        trainer.run(feed_dict={inputdata: _inputdata, particle_type: _label, weight: _weight, keep_prob: 0.5, is_training: True, mean:_mean}, session=sess)

        if i%100 == 0:
            _type_predict = particle_type_predict.eval(feed_dict={inputdata: _inputdata, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)
            cross_en_w_weight = loss_function.eval(feed_dict={inputdata: _inputdata, particle_type: _label, weight: _weight, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)
            cross_en_wo_weight = loss_function_wo_weight.eval(feed_dict={inputdata: _inputdata, particle_type: _label, weight: _weight, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)

            _type_predict_test = particle_type_predict.eval(feed_dict={inputdata: _test_inputdata, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)
            cross_en_w_weight_test = loss_function.eval(feed_dict={inputdata: _test_inputdata, particle_type: _test_label, weight: _test_weight, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)
            cross_en_wo_weight_test = loss_function_wo_weight.eval(feed_dict={inputdata: _test_inputdata, particle_type: _test_label, weight: _test_weight, keep_prob: 1.0, is_training:False, mean:_mean}, session=sess)

            prediction = np.ravel(np.array(_type_predict))#[:,1]
            weight_cal = np.ravel(_weight)
            score = scoring.rejection90(np.ravel(_label), prediction, weight_cal)*1e4

            prediction = np.ravel(np.array(_type_predict_test))#[:,1]
            weight_cal_test = np.ravel(_test_weight)
            score_test = scoring.rejection90(np.ravel(_test_label), prediction, weight_cal_test)*1e4

            array_cs_en_predict = np.round(np.array(_type_predict))
            array_label = np.array(_label)
            same = array_cs_en_predict == array_label
            percent = 1.0*same.sum()/len(same)

            array_test_cs_en_predict = np.round(np.array(_type_predict_test))
            array_test_label = np.array(_test_label)
            same_test = array_test_cs_en_predict == array_test_label
            percent_test = 1.0*same_test.sum()/len(same_test)

            print(i, cross_en_w_weight, cross_en_wo_weight, score, percent, cross_en_w_weight_test, cross_en_wo_weight_test, score_test, percent_test)
            outfiletxt.write(str(i))
            outfiletxt.write(" ")
            outfiletxt.write(str(cross_en_w_weight))
            outfiletxt.write(" ")
            outfiletxt.write(str(cross_en_wo_weight))
            outfiletxt.write(" ")
            outfiletxt.write(str(score))
            outfiletxt.write(" ")
            outfiletxt.write(str(percent))
            outfiletxt.write(" ")
            outfiletxt.write(str(cross_en_w_weight_test))
            outfiletxt.write(" ")
            outfiletxt.write(str(cross_en_wo_weight_test))
            outfiletxt.write(" ")
            outfiletxt.write(str(score_test))
            outfiletxt.write(" ")
            outfiletxt.write(str(percent_test))
            outfiletxt.write("\n")

        if boolhalfmuon == True and i%n_update == 0 :
            this_train_data = make_arranged_data(train_data_split, train_ratio_muon)

        if i%100 == 0:
            modelname = modeldir+"model_%d.ckpt"%(i)
            saver.save(sess, modelname)

    print("fin ...")

    sess.close()

def loadtest(modelname):
    pass

if __name__=="__main__":
    train()
