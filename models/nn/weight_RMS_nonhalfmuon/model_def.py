import numpy as np
import tensorflow as tf
from tf_func import *

learning_rate=1e-3
#learning_rate=1e-2
#learning_rate=1e-5
#learning_rate=0.5

#n_0 = 67 #same as ndim_data
n_0 = 79 + 8#same as ndim_data

def estimator(inputdata, keep_prob, is_training):

#1st layer
    n_1 = 256
    w_1 = weight_variable([n_0, n_1], "w_1", np.sqrt(2.0/n_0))
    b_1 = bias_variable([n_1], "b_1", np.sqrt(2.0/n_0))
    #x_1 = tf.nn.leaky_relu(tf.matmul(inputdata, w_1) + b_1)
    #x_1 = tf.nn.relu(tf.matmul(inputdata, w_1) + b_1)
    x_1 = tf.nn.sigmoid(tf.matmul(inputdata, w_1) + b_1)
    #x_1 = tf.nn.tanh(tf.matmul(inputdata, w_1) + b_1)

    x_1_bn = x_1
    #x_1_bn = tf.nn.dropout(x_1, keep_prob)
    #x_1_bn = tf.layers.batch_normalization(_x_1_bn, training=is_training)

##2st layer
    n_2 = 256
    w_2 = weight_variable([n_1, n_2], "w_2", np.sqrt(2.0/n_1))
    b_2 = bias_variable([n_2], "b_2", np.sqrt(2.0/n_1))
    #x_2 = tf.nn.leaky_relu(tf.matmul(x_1_bn, w_2) + b_2)
    #x_2 = tf.nn.relu(tf.matmul(x_1_bn, w_2) + b_2)
    x_2 = tf.nn.sigmoid(tf.matmul(x_1_bn, w_2) + b_2)
    #x_2 = tf.nn.tanh(tf.matmul(x_1_bn, w_2) + b_2)

    x_2_bn = x_2
    #x_2_bn = tf.nn.dropout(x_2, keep_prob)
    #x_2_bn = tf.layers.batch_normalization(_x_2_bn, training=is_training)

##3rd layer
    n_3 = 256
    w_3 = weight_variable([n_2, n_3], "w_3", np.sqrt(2.0/n_2))
    b_3 = bias_variable([n_3], "b_3", np.sqrt(2.0/n_2))
    #x_3 = tf.nn.leaky_relu(tf.matmul(x_2_bn, w_3) + b_3)
    #x_3 = tf.nn.relu(tf.matmul(x_2_bn, w_3) + b_3)
    x_3 = tf.nn.sigmoid(tf.matmul(x_2_bn, w_3) + b_3)
    #x_3 = tf.nn.tanh(tf.matmul(x_2_bn, w_3) + b_3)

    x_3_bn = tf.nn.dropout(x_3, keep_prob)
    #x_3_bn = tf.layers.batch_normalization(_x_3_bn, training=is_training)

#output layer
    n_out = 1
    w_out = weight_variable([n_3, n_out], "w_out", np.sqrt(2.0/n_3))
    b_out = bias_variable([n_out], "b_out", np.sqrt(2.0/n_3))
    x_out = tf.nn.sigmoid(tf.matmul(x_3_bn, w_out) + b_out)
    
    return x_out

#def loss_function(particle_type_predict, particle_type, weight):
def loss_function(particle_type_predict, particle_type, weight, mean): # mean: -25 ~ +25
    sigma = 8.0
    #return -1*tf.reduce_mean(weight*particle_type*tf.log(tf.clip_by_value(particle_type_predict, 1e-10, 1.0)))
    #return -1*tf.reduce_mean(weight*(particle_type*tf.log(tf.clip_by_value(particle_type_predict, 1e-10, 1.0)) + (1-particle_type)*tf.log(tf.clip_by_value((1-particle_type_predict), 1e-10, 1.0))))
    return -1*tf.reduce_mean(tf.nn.sigmoid((weight - mean)/sigma)*(particle_type*tf.log(tf.clip_by_value(particle_type_predict, 1e-10, 1.0)) + (1-particle_type)*tf.log(tf.clip_by_value((1-particle_type_predict), 1e-10, 1.0))))

def loss_function_wo_weight(particle_type_predict, particle_type, weight):
    #return -1*tf.reduce_mean(particle_type*tf.log(tf.clip_by_value(particle_type_predict, 1e-10, 1.0)))
    return -1*tf.reduce_mean(particle_type*tf.log(tf.clip_by_value(particle_type_predict, 1e-10, 1.0)) + (1-particle_type)*tf.log(tf.clip_by_value((1-particle_type_predict), 1e-10, 1.0)))

def trainer_rms(loss):
    #train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(loss)
    return train_step

def trainer_adam(loss):
    train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)
    return train_step
