import sys
sys.path.append("../../idao/yandex/")
import os
import h5py
import pandas as pd
import gc

import utils
import scoring
import calc_var as cv

#import model_def
import numpy as np
import tensorflow as tf
import model_def

from datetime import datetime

modeldir= "trained_pars/"

dir_dataname = "../../data/"

test_data = utils.load_test_csv(dir_dataname, utils.SIMPLE_FEATURE_COLUMNS)
test_data = (cv.MeasCalculator()).calc_all(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature_sq(test_data)
test_data = (cv.MeasCalculator()).calc_newfeature_v2(test_data)
test_data['dens_FOI_N0'] = (test_data['FOI_N0']/test_data['Mextra_DX2[0]']**0.5/test_data['Mextra_DY2[0]']**0.5)**0.25*4
test_data['dens_FOI_N1'] = (test_data['FOI_N1']/test_data['Mextra_DX2[1]']**0.5/test_data['Mextra_DY2[1]']**0.5)**0.25*4
test_data['dens_FOI_N2'] = (test_data['FOI_N2']/test_data['Mextra_DX2[2]']**0.5/test_data['Mextra_DY2[2]']**0.5)**0.25*4
test_data['dens_FOI_N3'] = (test_data['FOI_N3']/test_data['Mextra_DX2[3]']**0.5/test_data['Mextra_DY2[3]']**0.5)**0.25*4

ndim_data = 79 + 8

modelnamelist = ["trained_pars/model_100.ckpt", "trained_pars/model_200.ckpt", "trained_pars/model_300.ckpt", "trained_pars/model_400.ckpt", "trained_pars/model_500.ckpt", "trained_pars/model_600.ckpt", "trained_pars/model_700.ckpt", "trained_pars/model_800.ckpt", "trained_pars/model_900.ckpt", "trained_pars/model_1000.ckpt", "trained_pars/model_1100.ckpt", "trained_pars/model_1200.ckpt", "trained_pars/model_1300.ckpt", "trained_pars/model_1400.ckpt", "trained_pars/model_1500.ckpt", "trained_pars/model_1600.ckpt", "trained_pars/model_1700.ckpt", "trained_pars/model_1800.ckpt", "trained_pars/model_1900.ckpt", "trained_pars/model_2000.ckpt", "trained_pars/model_2100.ckpt", "trained_pars/model_2200.ckpt", "trained_pars/model_2300.ckpt", "trained_pars/model_2400.ckpt", "trained_pars/model_2500.ckpt", "trained_pars/model_2600.ckpt", "trained_pars/model_2700.ckpt", "trained_pars/model_2800.ckpt", "trained_pars/model_2900.ckpt", "trained_pars/model_3000.ckpt", "trained_pars/model_3100.ckpt", "trained_pars/model_3200.ckpt", "trained_pars/model_3300.ckpt", "trained_pars/model_3400.ckpt", "trained_pars/model_3500.ckpt", "trained_pars/model_3600.ckpt", "trained_pars/model_3700.ckpt", "trained_pars/model_3800.ckpt", "trained_pars/model_3900.ckpt", "trained_pars/model_4000.ckpt", "trained_pars/model_4100.ckpt", "trained_pars/model_4200.ckpt", "trained_pars/model_4300.ckpt", "trained_pars/model_4400.ckpt", "trained_pars/model_4500.ckpt", "trained_pars/model_4600.ckpt", "trained_pars/model_4700.ckpt", "trained_pars/model_4800.ckpt", "trained_pars/model_4900.ckpt", "trained_pars/model_5000.ckpt", "trained_pars/model_5100.ckpt", "trained_pars/model_5200.ckpt", "trained_pars/model_5300.ckpt", "trained_pars/model_5400.ckpt", "trained_pars/model_5500.ckpt", "trained_pars/model_5600.ckpt", "trained_pars/model_5700.ckpt", "trained_pars/model_5800.ckpt", "trained_pars/model_5900.ckpt", "trained_pars/model_6000.ckpt", "trained_pars/model_6100.ckpt", "trained_pars/model_6200.ckpt", "trained_pars/model_6300.ckpt", "trained_pars/model_6400.ckpt", "trained_pars/model_6500.ckpt", "trained_pars/model_6600.ckpt", "trained_pars/model_6700.ckpt", "trained_pars/model_6800.ckpt", "trained_pars/model_6900.ckpt", "trained_pars/model_7000.ckpt", "trained_pars/model_7100.ckpt", "trained_pars/model_7200.ckpt", "trained_pars/model_7300.ckpt", "trained_pars/model_7400.ckpt", "trained_pars/model_7500.ckpt", "trained_pars/model_7600.ckpt", "trained_pars/model_7700.ckpt", "trained_pars/model_7800.ckpt", "trained_pars/model_7900.ckpt", "trained_pars/model_8000.ckpt", "trained_pars/model_8100.ckpt", "trained_pars/model_8200.ckpt", "trained_pars/model_8300.ckpt", "trained_pars/model_8400.ckpt", "trained_pars/model_8500.ckpt", "trained_pars/model_8600.ckpt", "trained_pars/model_8700.ckpt", "trained_pars/model_8800.ckpt", "trained_pars/model_8900.ckpt", "trained_pars/model_9000.ckpt", "trained_pars/model_9100.ckpt", "trained_pars/model_9200.ckpt", "trained_pars/model_9300.ckpt", "trained_pars/model_9400.ckpt", "trained_pars/model_9500.ckpt", "trained_pars/model_9600.ckpt", "trained_pars/model_9700.ckpt", "trained_pars/model_9800.ckpt", "trained_pars/model_9900.ckpt", "trained_pars/model_10000.ckpt"]

def get_inputdata(data):
    sample = data
    
    match_r = sample[["MatchedHit_R_0" ,"MatchedHit_R_1" ,"MatchedHit_R_2" ,"MatchedHit_R_3"]]/5000.0
    region = sample[['region_0', 'region_1', 'region_2', 'region_3']]/4.0

    station0 = sample[['sq_chi_physX0', 'sq_chi_modelX0', 'sq_chi_physY0', 'sq_chi_modelY0']].values
    station1 = sample[['sq_chi_physX1', 'sq_chi_modelX1', 'sq_chi_physY1', 'sq_chi_modelY1']].values
    station2 = sample[['sq_chi_physX2', 'sq_chi_modelX2', 'sq_chi_physY2', 'sq_chi_modelY2']].values
    station3 = sample[['sq_chi_physX3', 'sq_chi_modelX3', 'sq_chi_physY3', 'sq_chi_modelY3']].values
    
    asd3 = sample[['AverageSquaredDistance3']].values
    chi_root_X_phys = sample[['chi_root_physX0']].values + sample[['chi_root_physX1']].values + sample[['chi_root_physX2']].values + sample[['chi_root_physX3']].values
    chi_root_Y_phys = sample[['chi_root_physY0']].values + sample[['chi_root_physY1']].values + sample[['chi_root_physY2']].values + sample[['chi_root_physY3']].values
    chi_root_X_model = sample[['chi_root_modelX0']].values + sample[['chi_root_modelX1']].values + sample[['chi_root_modelX2']].values + sample[['chi_root_modelX3']].values
    chi_root_Y_model = sample[['chi_root_modelY0']].values + sample[['chi_root_modelY1']].values + sample[['chi_root_modelY2']].values + sample[['chi_root_modelY3']].values
#21

    hitType = sample[['MatchedHit_TYPE[0]', 'MatchedHit_TYPE[1]', 'MatchedHit_TYPE[2]', 'MatchedHit_TYPE[3]']].values
    measX = sample[['MatchedHit_X[0]', 'MatchedHit_X[1]', 'MatchedHit_X[2]', 'MatchedHit_X[3]']].values/2000
    measY = sample[['MatchedHit_Y[0]', 'MatchedHit_Y[1]', 'MatchedHit_Y[2]', 'MatchedHit_Y[3]']].values/2000
    measZ0 = ((sample[['MatchedHit_Z[0]']]-15250)/150).values
    measZ1 = ((sample[['MatchedHit_Z[1]']]-16450)/150).values
    measZ2 = ((sample[['MatchedHit_Z[2]']]-17650)/150).values
    measZ3 = ((sample[['MatchedHit_Z[3]']]-18850)/150).values
    measZ = np.concatenate((measZ0, measZ1, measZ2, measZ3), axis=1)
    measT = sample[['MatchedHit_T[0]', 'MatchedHit_T[1]', 'MatchedHit_T[2]', 'MatchedHit_T[3]']].values/5
#20

    del_measX = 10.0/sample[['MatchedHit_DX[0]', 'MatchedHit_DX[1]', 'MatchedHit_DX[2]', 'MatchedHit_DX[3]']].values
    del_measY = 10.0/sample[['MatchedHit_DY[0]', 'MatchedHit_DY[1]', 'MatchedHit_DY[2]', 'MatchedHit_DY[3]']].values
    del_measZ = 10.0/sample[['MatchedHit_DZ[0]', 'MatchedHit_DZ[1]', 'MatchedHit_DZ[2]', 'MatchedHit_DZ[3]']].values
    del_measT = sample[['MatchedHit_DT[0]', 'MatchedHit_DT[1]', 'MatchedHit_DT[2]', 'MatchedHit_DT[3]']].values/5
    del_modelX = 10.0/(sample[['Mextra_DX2[0]', 'Mextra_DX2[1]', 'Mextra_DX2[2]', 'Mextra_DX2[3]']].values)**0.5
    del_modelY = 10.0/(sample[['Mextra_DY2[0]', 'Mextra_DY2[1]', 'Mextra_DY2[2]', 'Mextra_DY2[3]']].values)**0.5
#24

    PT = sample[['sq_PT']].values
    P = sample[['sq_P']].values
    PRatio = sample[['PRatio']].values
    PCat = sample[['PCat']].values/8.0
    FOI_N0 = (sample[['FOI_N0']].values - 2)/3.0
    FOI_N1 = (sample[['FOI_N1']].values - 2)/3.0
    FOI_N2 = (sample[['FOI_N2']].values - 2)/3.0
    FOI_N3 = (sample[['FOI_N3']].values - 2)/3.0
    NShared = sample[['NShared']].values/7.0
    ndof = (sample[['ndof']].values - 4)/4.0
#10
    dens_FOI = sample[['dens_FOI_N0', 'dens_FOI_N1', 'dens_FOI_N2', 'dens_FOI_N3']].values
#4
    inputdata = np.concatenate((match_r ,region ,station0 ,station1 ,station2 ,station3 ,asd3 ,chi_root_X_phys ,chi_root_Y_phys ,chi_root_X_model ,chi_root_Y_model ,hitType ,measX ,measY ,measZ ,measT ,del_measX ,del_measY ,del_measZ ,del_measT ,del_modelX ,del_modelY ,PT ,P ,PRatio ,PCat ,FOI_N0 ,FOI_N1 ,FOI_N2 ,FOI_N3 ,NShared ,ndof ,dens_FOI) , axis=1)

    return inputdata

def predict():
#input
    keep_prob = tf.placeholder(tf.float32)
    is_training = tf.placeholder(dtype=bool)
    inputdata = tf.placeholder(tf.float32, [None, ndim_data])

#output
    particle_type = tf.placeholder(tf.float32, [None, 1])

#estimator
    particle_type_predict = model_def.estimator(inputdata, keep_prob, is_training) 

    config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver(max_to_keep=None)

    _inputdata = get_inputdata(test_data)

    for modelname in modelnamelist:
        saver.restore(sess, modelname)

        _type_predict = particle_type_predict.eval(feed_dict={inputdata: _inputdata, keep_prob: 1.0, is_training:False}, session=sess)

        array_type_predict = np.ravel(np.array(_type_predict))
        
        outfile = open(modelname+"_fin.csv", "w")
        outline = "id,prediction"
        print(outline)
        outfile.write(outline)
        outfile.write("\n")
        for i in range(len(array_type_predict)):
            outline = str(i)+","+str(array_type_predict[i])
            outfile.write(outline)
            outfile.write("\n")

    sess.close()

if __name__=="__main__":
    predict()
