#!/usr/bin/env python
# coding: UTF-8

import sys
import pandas as pd
import numpy as np

filelist = [ "trained_pars/model_100.ckpt_fin.csv" ,"trained_pars/model_1000.ckpt_fin.csv" ,"trained_pars/model_10000.ckpt_fin.csv" ,"trained_pars/model_1100.ckpt_fin.csv" ,"trained_pars/model_1200.ckpt_fin.csv" ,"trained_pars/model_1300.ckpt_fin.csv" ,"trained_pars/model_1400.ckpt_fin.csv" ,"trained_pars/model_1500.ckpt_fin.csv" ,"trained_pars/model_1600.ckpt_fin.csv" ,"trained_pars/model_1700.ckpt_fin.csv" ,"trained_pars/model_1800.ckpt_fin.csv" ,"trained_pars/model_1900.ckpt_fin.csv" ,"trained_pars/model_200.ckpt_fin.csv" ,"trained_pars/model_2000.ckpt_fin.csv" ,"trained_pars/model_2100.ckpt_fin.csv" ,"trained_pars/model_2200.ckpt_fin.csv" ,"trained_pars/model_2300.ckpt_fin.csv" ,"trained_pars/model_2400.ckpt_fin.csv" ,"trained_pars/model_2500.ckpt_fin.csv" ,"trained_pars/model_2600.ckpt_fin.csv" ,"trained_pars/model_2700.ckpt_fin.csv" ,"trained_pars/model_2800.ckpt_fin.csv" ,"trained_pars/model_2900.ckpt_fin.csv" ,"trained_pars/model_300.ckpt_fin.csv" ,"trained_pars/model_3000.ckpt_fin.csv" ,"trained_pars/model_3100.ckpt_fin.csv" ,"trained_pars/model_3200.ckpt_fin.csv" ,"trained_pars/model_3300.ckpt_fin.csv" ,"trained_pars/model_3400.ckpt_fin.csv" ,"trained_pars/model_3500.ckpt_fin.csv" ,"trained_pars/model_3600.ckpt_fin.csv" ,"trained_pars/model_3700.ckpt_fin.csv" ,"trained_pars/model_3800.ckpt_fin.csv" ,"trained_pars/model_3900.ckpt_fin.csv" ,"trained_pars/model_400.ckpt_fin.csv" ,"trained_pars/model_4000.ckpt_fin.csv" ,"trained_pars/model_4100.ckpt_fin.csv" ,"trained_pars/model_4200.ckpt_fin.csv" ,"trained_pars/model_4300.ckpt_fin.csv" ,"trained_pars/model_4400.ckpt_fin.csv" ,"trained_pars/model_4500.ckpt_fin.csv" ,"trained_pars/model_4600.ckpt_fin.csv" ,"trained_pars/model_4700.ckpt_fin.csv" ,"trained_pars/model_4800.ckpt_fin.csv" ,"trained_pars/model_4900.ckpt_fin.csv" ,"trained_pars/model_500.ckpt_fin.csv" ,"trained_pars/model_5000.ckpt_fin.csv" ,"trained_pars/model_5100.ckpt_fin.csv" ,"trained_pars/model_5200.ckpt_fin.csv" ,"trained_pars/model_5300.ckpt_fin.csv" ,"trained_pars/model_5400.ckpt_fin.csv" ,"trained_pars/model_5500.ckpt_fin.csv" ,"trained_pars/model_5600.ckpt_fin.csv" ,"trained_pars/model_5700.ckpt_fin.csv" ,"trained_pars/model_5800.ckpt_fin.csv" ,"trained_pars/model_5900.ckpt_fin.csv" ,"trained_pars/model_600.ckpt_fin.csv" ,"trained_pars/model_6000.ckpt_fin.csv" ,"trained_pars/model_6100.ckpt_fin.csv" ,"trained_pars/model_6200.ckpt_fin.csv" ,"trained_pars/model_6300.ckpt_fin.csv" ,"trained_pars/model_6400.ckpt_fin.csv" ,"trained_pars/model_6500.ckpt_fin.csv" ,"trained_pars/model_6600.ckpt_fin.csv" ,"trained_pars/model_6700.ckpt_fin.csv" ,"trained_pars/model_6800.ckpt_fin.csv" ,"trained_pars/model_6900.ckpt_fin.csv" ,"trained_pars/model_700.ckpt_fin.csv" ,"trained_pars/model_7000.ckpt_fin.csv" ,"trained_pars/model_7100.ckpt_fin.csv" ,"trained_pars/model_7200.ckpt_fin.csv" ,"trained_pars/model_7300.ckpt_fin.csv" ,"trained_pars/model_7400.ckpt_fin.csv" ,"trained_pars/model_7500.ckpt_fin.csv" ,"trained_pars/model_7600.ckpt_fin.csv" ,"trained_pars/model_7700.ckpt_fin.csv" ,"trained_pars/model_7800.ckpt_fin.csv" ,"trained_pars/model_7900.ckpt_fin.csv" ,"trained_pars/model_800.ckpt_fin.csv" ,"trained_pars/model_8000.ckpt_fin.csv" ,"trained_pars/model_8100.ckpt_fin.csv" ,"trained_pars/model_8200.ckpt_fin.csv" ,"trained_pars/model_8300.ckpt_fin.csv" ,"trained_pars/model_8400.ckpt_fin.csv" ,"trained_pars/model_8500.ckpt_fin.csv" ,"trained_pars/model_8600.ckpt_fin.csv" ,"trained_pars/model_8700.ckpt_fin.csv" ,"trained_pars/model_8800.ckpt_fin.csv" ,"trained_pars/model_8900.ckpt_fin.csv" ,"trained_pars/model_900.ckpt_fin.csv" ,"trained_pars/model_9000.ckpt_fin.csv" ,"trained_pars/model_9100.ckpt_fin.csv" ,"trained_pars/model_9200.ckpt_fin.csv" ,"trained_pars/model_9300.ckpt_fin.csv" ,"trained_pars/model_9400.ckpt_fin.csv" ,"trained_pars/model_9500.ckpt_fin.csv" ,"trained_pars/model_9600.ckpt_fin.csv" ,"trained_pars/model_9700.ckpt_fin.csv" ,"trained_pars/model_9800.ckpt_fin.csv" ,"trained_pars/model_9900.ckpt_fin.csv"]

outname = "weight_RMS_nonhalfmuon_average_all_fin.csv"

def main():
    csvfile = pd.read_csv(filelist[0])
    data = np.array(csvfile['prediction'])
    for i in range(1, len(filelist)):
        csvfile = pd.read_csv(filelist[i])
        data += np.array(csvfile['prediction'])

    outfile = open(outname, "w")
    outfile.write("id,prediction\n")

    for i in range(len(data)):
        outline = str(i) + "," + str(data[i]/len(filelist)) + "\n"
        outfile.write(outline)

if __name__=="__main__":
    main()

