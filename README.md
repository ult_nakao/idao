# IDAO
* This repository is for IDAO.
* See [wiki](https://bitbucket.org/ult_nakao/idao/wiki/Home) in detail (in Japanese).

## Team member
- NAKAO, Mitsutaka
- Onda, Rina
- YONEDA, Hiroki

## Folder description
- cpp: cpp implementation of features for Track2
- env: list of python modules used in this repository
- models: trained models
- notebook: try and error, learning code for XGBoost, CatBoost
- predictions: the final submission for Track1
- ref: references
- stacking: data used for stacking (but we did not used stacking for the final submission)
- test: test files for Track2 submission
- track2: codes used for Track2
- yandex: python implementation used for this contest. Part of them is git-pulled from the common repository (utils.py, scoring.py), the others are implemented by ourselves.

## Models
### Feature engineering
- Most of the features are implemented in yandex/calc_var.py
- Part of them is also implemented in cpp for Track2.

### Track1
- We averaged three different models: XGBoost-based, CatBoost-based, and NN-based.
- Not the same input features are used in the following models.
- XGBoost-based model
    - training & prediction: notebook/track1_xgboost.ipynb 
    - trained model: models/XGB_20190208_9_22_0.pkl, XGB_20190208_9_22_1.pkl
- CatBoost-based model
    - training: notebook/track1_catboost.ipynb
    - prediction: notebook/submit_catboost.ipynb
    - trained model: models/Cat_20190210_nn_full.cbm
- NN-based model
    - models/nn
    - the procedure of the training and prediction is described in 'run.txt'.

- Finally, we averaged these models above using yandex/ensemble.py
### Track2
- We used CatBoost for Track2.
- learning: track2_catboost.ipynb
- Then, we implemented these features in cpp and make a submission zip.
- The final submission: track2/submit/cat0
