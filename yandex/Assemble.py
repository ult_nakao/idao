import sys
from itertools import repeat
import numpy as np
import pandas as pd

ID_COLUMN = "id"

FILE_PATH = './track_2_baseline_my_python'
FILE_NAMES = ['submmision_train2.csv','submmision_train2_GB.csv']

def main():
    first = True
    for filename in FILE_NAMES:
        data = pd.read_csv(FILE_PATH + '/' + filename, usecols=[ID_COLUMN] + ["prediction"],
                           index_col=ID_COLUMN)
        if(first):
            index = data.index
            assembly = np.array([data['prediction'].values])
            first = False
        else:
            assembly = np.append(assembly, np.array([data['prediction'].values]), axis=0)


    mean = np.mean(assembly, axis=0)
    pd.DataFrame(data={"prediction": mean}, index=index).to_csv(
                    'assembly.csv', index_label=ID_COLUMN, header=True)


if __name__ == "__main__":
    main()
