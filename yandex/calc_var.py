"""
Usage:
import calc_var as cv
event = (cv.MeasCalculator()).calc_*(event)
"""

import numpy as np
import pandas as pd

class MeasCalculator:
    def __init__(self):
        return

    def calc_all(self, events):
        events = self.calc_PCat(events)
        events = self.calc_PRatio(events)
        events = self.calc_PL(events)
        events = self.calc_DiffMatchedLextra(events)
        events = self.calc_AverageSquaredDistance3(events)
        #events = self.calc_newfeature_v2(events)
        events = self.calc_region(events)
        events = self.calc_matched_r(events)
        #events = self.calc_newfeature(events)
        #events = self.calc_newfeature_sq(events)
        return events

    def calc_PCat(self, events):

        mask = events['P'] < 3000
        mask = mask + 0
        thres = [6000, 8000, 10000, 20000, 30000, 40000, 50000, 60000] 
        for thre in thres:
            mask_tmp = events['P'] > thre
            mask_tmp = mask_tmp + 0
            mask = mask + mask_tmp

        tmp = pd.DataFrame(mask)
        events['PCat'] = tmp['P']
        return events
    
    def calc_PRatio(self, events):

        events['PRatio'] = events['PT'] / events['P']
        return events

    def calc_PL(self, events):

        events['PL'] = np.sqrt(events['P'] * events['P'] - events['PT'] * events['PT'])
        return events

    def calc_DiffMatchedLextra(self, events):
        nameMatched = "MatchedHit_"
        nameLextra  = "Lextra_"
        nameDiff    = "DiffMatchedLextra_"
        for xy in ['X', 'Y']:
            for station in [0, 1, 2, 3]:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDiff    = nameDiff     + xy + '[' + str(station) + ']'
                events[aNameDiff] = events[aNameMatched] - events[aNameLextra]
        
        return events

    def calc_AverageSquaredDistance(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/events[aNameDelta])**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_AverageSquaredDistance2(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance2" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**2 + events[aNameDeltaEx])**0.5)**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD2"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_AverageSquaredDistance3(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance3" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**0.5*events[aNameDeltaEx]**0.25))**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD3"
        events[nameInvASD] = 1.0/events[nameASD]
        
        nameLogInvASD = "Log" + nameInvASD
        events[nameLogInvASD] = np.log(events[nameInvASD])

        return events

    def calc_AverageSquaredDistance4(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        nameASD    = "AverageSquaredDistance4" 
        nameNhit    = "Nhit" 
        events[nameASD] = 0
        events[nameNhit] = 0

        for station in [0, 1, 2, 3]:
            aNameType = nameType + '[' + str(station) + ']'
            events.loc[~(events[aNameType] == 0), nameNhit] += 1
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'

                events.loc[~(events[aNameType] == 0), nameASD] += ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**(-1) + events[aNameDeltaEx]**(-0.5))**(-1))**2

        events[nameASD] /= events[nameNhit]

        nameInvASD    = "InvASD4"
        events[nameInvASD] = 1.0/events[nameASD]
        
        return events

    def calc_IsMuon(self, events):
        nameMuon = 'IsMuon'
        events[nameMuon] = 0

        mask = (3000.0 <= events['P'])
        mask = mask & (events['P'] < 6000.0)
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        events.loc[mask, nameMuon] = 1

        mask = (6000.0 <= events['P'])
        mask = mask & (events['P'] < 10000.0)
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        mask = mask & ((events['MatchedHit_TYPE[2]'] > 0) | (events['MatchedHit_TYPE[3]'] > 0))
        events.loc[mask, nameMuon] = 1

        mask = (10000.0 <= events['P'])
        mask = mask & (events['MatchedHit_TYPE[0]'] > 0)
        mask = mask & (events['MatchedHit_TYPE[1]'] > 0)
        mask = mask & ((events['MatchedHit_TYPE[2]'] > 0) & (events['MatchedHit_TYPE[3]'] > 0))
        events.loc[mask, nameMuon] = 1

        return events

    def calc_newfeature(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        for station in [0, 1, 2, 3]:
            for xy in ['X', 'Y']:
                newName_phys = "chi_phys"+xy+str(station)
                newName_model = "chi_model"+xy+str(station)
                newName_both = "chi_both"+xy+str(station)

                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'
                aNameType = nameType + '[' + str(station) + ']'

                events[newName_phys] = 0
                events[newName_model] = 0
                events[newName_both] = 0

                events[newName_phys] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]))**2
                events[newName_model] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDeltaEx]**0.5))**2
                events[newName_both] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**2 + events[aNameDeltaEx])**0.5)**2

                events.loc[(events[aNameType] == 0), newName_phys] = 0
                events.loc[(events[aNameType] == 0), newName_model] = 0
                events.loc[(events[aNameType] == 0), newName_both] = 0

        events['FOI_N0'] = np.array([[x.count('0')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N1'] = np.array([[x.count('1')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N2'] = np.array([[x.count('2')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N3'] = np.array([[x.count('3')] for x in np.array(events['FOI_hits_S'])])
        
        return events

    def calc_newfeature_sq(self, events):
        pl=0.4
        for station in [0, 1, 2, 3]:
            for xy in ['X', 'Y']:
                Name_phys = "chi_phys"+xy+str(station)
                Name_model = "chi_model"+xy+str(station)
                Name_both = "chi_both"+xy+str(station)

                newName_phys = "sq_chi_phys"+xy+str(station)
                newName_model = "sq_chi_model"+xy+str(station)
                newName_both = "sq_chi_both"+xy+str(station)

                events[newName_phys] = 0
                events[newName_model] = 0
                events[newName_both] = 0

                events[newName_phys] = events[Name_phys]**pl
                events[newName_model] = events[Name_model]**pl
                events[newName_both] = events[Name_both]**pl
        
        events['sq_PT'] = 0
        events['sq_P'] = 0

        events['sq_PT'] = (events['PT'] - 800)**0.5/20
        events['sq_P'] = (events['P'] - 3000)**0.5/100

        return events

    def calc_newfeature_v2(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
        nameLextra  = "Lextra_" #x_extrapolation_i
        nameDelta   = "MatchedHit_D" #pad-size
        nameDeltaEx   = "Mextra_D" #pad-size
        nameType = "MatchedHit_TYPE"

        for station in [0, 1, 2, 3]:
            for xy in ['X', 'Y']:
                newName_phys = "chi_root_phys"+xy+str(station)
                newName_model = "chi_root_model"+xy+str(station)
                newName_both = "chi_root_both"+xy+str(station)

                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                aNameLextra  = nameLextra   + xy + '[' + str(station) + ']'
                aNameDelta   = nameDelta    + xy + '[' + str(station) + ']'
                aNameDeltaEx = nameDeltaEx  + xy + '2[' + str(station) + ']'
                aNameType = nameType + '[' + str(station) + ']'

                events[newName_phys] = 0
                events[newName_model] = 0
                events[newName_both] = 0

                events[newName_phys] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]))
                events[newName_model] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDeltaEx]**0.5))
                events[newName_both] = ((events[aNameMatched] - events[aNameLextra])/(events[aNameDelta]**2 + events[aNameDeltaEx])**0.5)

                events.loc[(events[aNameType] == 0), newName_phys] = 0
                events.loc[(events[aNameType] == 0), newName_model] = 0
                events.loc[(events[aNameType] == 0), newName_both] = 0

        events['FOI_N0'] = np.array([[x.count('0')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N1'] = np.array([[x.count('1')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N2'] = np.array([[x.count('2')] for x in np.array(events['FOI_hits_S'])])
        events['FOI_N3'] = np.array([[x.count('3')] for x in np.array(events['FOI_hits_S'])])

        events['sq_PT'] = 0
        events['sq_P'] = 0
        events['sq_PT'] = (events['PT'] - 800)**0.5/20
        events['sq_P'] = (events['P'] - 3000)**0.5/100
        
        return events

    def calc_meta_sigma(self, events):
        events['sigma'] = 0
        events['sigma_0'] = 0
        events['sigma_1'] = 0
        events['sigma_2'] = 0
        events['sigma_3'] = 0
        for station in [0, 1, 2, 3]:
            aNewName = "sigma_" + str(station)
            for xy in ['x', 'y']:
                aName = "sigma_" + str(xy) +'_' + str(station)
                events[aNewName]    += events[aName] * events[aName]
                events['sigma']     += events[aName] * events[aName]
        
            events[aNewName] = np.sqrt(events[aNewName])
        
        events['sigma'] = np.sqrt(events['sigma'])

        return events

    def calc_matched_r(self, events):
        nameMatched = "MatchedHit_" #x_closest_i
         
        for station in [0, 1, 2, 3]:
            aNewName = "MatchedHit_R_" + str(station)
            events[aNewName] = 0
            for xy in ['X', 'Y']:
                aNameMatched = nameMatched  + xy + '[' + str(station) + ']'
                events[aNewName] += events[aNameMatched] * events[aNameMatched]
            
            events[aNewName] = np.sqrt(events[aNewName])
        
        events["MatchedHit_R_01"] = events["MatchedHit_R_1"] - events["MatchedHit_R_0"]
        events["MatchedHit_R_12"] = events["MatchedHit_R_2"] - events["MatchedHit_R_1"]
        events["MatchedHit_R_23"] = events["MatchedHit_R_3"] - events["MatchedHit_R_2"]

        return events

    def calc_region(self, events):
        
        for station in [0, 1, 2, 3]:
            aNewName = "region_" + str(station)
            events[aNewName] = 0
            nameMatchedX = "MatchedHit_X[" + str(station) + ']'
            nameMatchedY = "MatchedHit_Y[" + str(station) + ']'
            events.loc[(np.abs(events[nameMatchedX]) < 600) 
                    & (np.abs(events[nameMatchedY]) < 500), aNewName] += 1
            events.loc[(np.abs(events[nameMatchedX]) < 1200) 
                    & (np.abs(events[nameMatchedY]) < 1000), aNewName] += 1
            events.loc[(np.abs(events[nameMatchedX]) < 2400) 
                    & (np.abs(events[nameMatchedY]) < 2001), aNewName] += 1
            events.loc[(np.abs(events[nameMatchedX]) <= np.max(events[nameMatchedX])) 
                    & (np.abs(events[nameMatchedY]) <= np.max(events[nameMatchedY])), aNewName] += 1
            
            events[aNewName] = np.abs(events[aNewName] - 5)
            events.loc[events[nameMatchedX] == -9999, aNewName] = 0
            events.loc[events[nameMatchedY] == -9999, aNewName] = 0
        
        return events

    def check_MatchedHitPattern(self,events):
        events['MatchedHitPattern1'] = 0
        events['MatchedHitPattern2'] = 0
        events.loc[events['MatchedHit_TYPE[0]']+events['MatchedHit_TYPE[1]'] == 2,'MatchedHitPattern1'] = 1
        events.loc[events['MatchedHit_TYPE[0]']+events['MatchedHit_TYPE[1]'] == 3,'MatchedHitPattern1'] = 2
        events.loc[events['MatchedHit_TYPE[2]']+events['MatchedHit_TYPE[3]'] == 2, 'MatchedHitPattern2'] = 1
        events.loc[events['MatchedHit_TYPE[2]']+events['MatchedHit_TYPE[3]'] == 3, 'MatchedHitPattern2'] = 2
        events.loc[(events['MatchedHit_TYPE[2]']<1.5) & (events['MatchedHit_TYPE[3]']<1.5), 'MatchedHitPattern2'] = 3
        
        return events

    def calc_PCatRegion(self, events):
        events['PCatRegion'] = events['PCat'] * 4 + round((events['region_0'] + events['region_1'] + events['region_2'] + events['region_3'])/4) - 1

        return events

class RowWiseCalculator:
    def __init__(self):
        return

    def calc_all(self, row):
        row = self.calc_sigma_x(row)
        row = self.calc_sigma_y(row)
        return row

    def calc_sigma_x(self, row):
        hitsS = np.array([int(x) for x in  row['FOI_hits_S'].strip('[]').split()])
        hitsX = np.array([float(x) for x in  row['FOI_hits_X'].strip('[]').split()])
        for iSt in range(4):
            aName = "sigma_x_" + str(iSt)
            if not any(hitsS == iSt):
                row[aName] = -1
            else :
                row[aName] = np.std(hitsX[hitsS == iSt])
        return row
    
    def calc_sigma_y(self, row):
        hitsS = np.array([int(x) for x in  row['FOI_hits_S'].strip('[]').split()])
        hitsY = np.array([float(x) for x in  row['FOI_hits_Y'].strip('[]').split()])
        for iSt in range(4):
            aName = "sigma_y_" + str(iSt)
            if not any(hitsS == iSt):
                row[aName] = -1
            else :
                row[aName] = np.std(hitsY[hitsS == iSt])
        return row
