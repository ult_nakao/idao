#!/usr/bin/env python
# coding: UTF-8

import sys
sys.path.append("../idao/yandex/")
import os
import pandas as pd

import utils
import scoring
import calc_var as cv

import numpy as np

def main():
    dir_dataname = "../your_data_dir/"
    col_list = utils.SIMPLE_FEATURE_COLUMNS
    train_data, test_data = utils.load_data_csv(dir_dataname, col_list)
    test_data = (cv.MeasCalculator()).calc_AverageSquaredDistance3(test_data)
    test_data['prediction'] = 1.0/test_data['AverageSquaredDistance3']
    outdata = test_data[['prediction']]
    outdata.to_csv('inv_asd3.csv')

if __name__=="__main__":
    main()

