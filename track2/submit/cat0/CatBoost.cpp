// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>

#include "./parser.h"
#include "./calc_var.h"
#include "ripped_evaluator/evaluator.h"

const size_t N_MY_FEATURES = 36;

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    const std::string MODEL_FILE = "Cat_20190209_0.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator(MODEL_FILE);

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
    
      std::vector<double> features(N_FEATURES);
      size_t id;
      ugly_hardcoded_parse(std::cin, &id, &features);
      
      std::vector<double> my_features(N_MY_FEATURES);
      my_features[0] = features[FOIHitsNId];//FOI_hits_N
      my_features[1] = features[PTId];//PT
      my_features[2] = features[PId];//P
      my_features[3] = calc_PCat(features);//PCat
      my_features[4] = calc_PRatio(features);//PRatio
      double tmp0[4], tmp1[4], tmp2[4], tmp3[4];
      calc_DiffMatchedLextra(features,tmp0,tmp1);
      for(int i=0; i<4; ++i){
	 my_features[5+i] = tmp0[i];//DiffMatchedLextra_X
	 my_features[9+i] = tmp1[i];//DiffMatchedLextra_Y
      }
      my_features[13] = calc_AverageSquaredDistanceInv(features);//invASD
      calc_ChiPhys(features,tmp0,tmp1);
      calc_ChiModel(features,tmp2,tmp3);
      for(int i=0; i<4; ++i){
	 my_features[14+i*4] = tmp0[i];//chi_phys_X
	 my_features[15+i*4] = tmp2[i];//chi_model_X
	 my_features[16+i*4] = tmp1[i];//chi_phys_Y
	 my_features[17+i*4] = tmp3[i];//chi_model_Y
	 my_features[30+i] = features[FOINId[i]];//FOI_N
      }
      my_features[34] = calc_sqPT(features);//sq_PT
      my_features[35] = calc_sqP(features);//sq_P

      const double prediction = \
            evaluator.Apply(my_features, NCatboostStandalone::EPredictionType::RawValue);
	    
      std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
