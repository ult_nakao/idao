#include "./calc_var.h"

double calc_AverageSquaredDistanceInv(const std::vector<double> &features){
   int nevents = 0;
   double ASD = 0;

   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      ++nevents;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double MatchedHitDX = features[MatchedHitDXId[istation]];
      const double MatchedHitDY = features[MatchedHitDYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      ASD += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/(MatchedHitDX*sqrt(MextraDX));
      ASD += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/(MatchedHitDY*sqrt(MextraDY));
   }
   return nevents/ASD;
}

double calc_AverageSquaredDistance(const std::vector<double> &features){
   int nevents = 0;
   double ASD = 0;

   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      ++nevents;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double MatchedHitDX = features[MatchedHitDXId[istation]];
      const double MatchedHitDY = features[MatchedHitDYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      ASD += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/(MatchedHitDX*sqrt(MextraDX));
      ASD += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/(MatchedHitDY*sqrt(MextraDY));
   }
   return ASD/nevents;
}


int check_MatchedHitPattern1(const std::vector<double> &features){
   if(features[MatchedHitTypeId[0]]+features[MatchedHitTypeId[1]]==2) return 1;//1&1
   else if(features[MatchedHitTypeId[0]]+features[MatchedHitTypeId[1]]==3) return 2;//1&2 or 2&1
   return 0;//2&2
}
   
int check_MatchedHitPattern2(const std::vector<double> &features){
   if((features[MatchedHitTypeId[2]]+features[MatchedHitTypeId[3]]==2) 
      && (features[MatchedHitTypeId[2]]*features[MatchedHitTypeId[3]]==0)) return 1;//0&2 or 2&0
   else if(features[MatchedHitTypeId[2]]+features[MatchedHitTypeId[3]]==3) return 2;//1&2 or 2&1
   else if(features[MatchedHitTypeId[2]]+features[MatchedHitTypeId[3]]<3) return 3;//0&0 or 0&1 or 1&0
   return 0;//2&2
}

void calc_sigma_x(const std::vector<double> &features, double *FOI_hits_X){
   const int FOI_hits_N = features[FOIHitsNId];
   for(int istation=0; istation<nstations; ++istation){
      int nhits = 0;
      double mean = 0;
      for(int ihit=0; ihit<FOI_hits_N; ++ihit){
	 int FOI_hits_S = features[FOIHitsNId];
	 if(istation==FOI_hits_S){
	    mean += features[FOIHitsNId];
	    ++nhits;
	 }
      }
      mean /= nhits>0 ? nhits : 1;
      double sigma = 0;
      for(int ihit=0; ihit<FOI_hits_N; ++ihit){
	 int FOI_hits_S = features[FOIHitsNId];
	 if(istation==FOI_hits_S){
	    sigma += (mean - features[FOIHitsNId])*(mean - features[FOIHitsNId]);
	 }
      }
      FOI_hits_X[istation] = nhits>0 ? sqrt(sigma / nhits) : -1;
   }

   return;
}

int calc_PCat(const std::vector<double> &features){
   double thrs[] = {6e3, 8e3, 1e4, 2e4, 3e4, 4e4, 5e4, 6e4};
   int nthrs = sizeof(thrs)/sizeof(double);
   int cat = 0;
   double P = features[PId];
   for(int ithr = 0; ithr<nthrs; ++ithr){
      if(P>thrs[ithr]) cat = ithr + 1;
   }
   return cat;
}

double calc_PRatio(const std::vector<double> &features){
   return features[PTId] / features[PId];
}

void calc_MatchedHitR(const std::vector<double> &features, double *R){
    
   if(sizeof(*R)/sizeof(double) < nstations) return;
   for(int istation=0; istation<nstations; ++istation){
      double MatchedHitX = features[MatchedHitXId[istation]];
      double MatchedHitY = features[MatchedHitYId[istation]];
 
      R[istation] = sqrt(MatchedHitX*MatchedHitX + MatchedHitY*MatchedHitY);
   }
   return;
}

void calc_MatchedHitRdiff(const std::vector<double> &features, double *R){
    
   if(sizeof(*R)/sizeof(double) < nstations-1) return;
   for(int istation=0; istation<nstations-1; ++istation){
      int jstation = istation + 1;
      double MatchedHitX1 = features[MatchedHitXId[istation]];
      double MatchedHitY1 = features[MatchedHitYId[istation]];
      double MatchedHitX2 = features[MatchedHitXId[jstation]];
      double MatchedHitY2 = features[MatchedHitYId[jstation]];
 
      R[istation] = - sqrt(MatchedHitX1*MatchedHitX1 + MatchedHitY1*MatchedHitY1);
      R[istation] +=  sqrt(MatchedHitX2*MatchedHitX2 + MatchedHitY2*MatchedHitY2);
   }
   return;
}

void calc_region(const std::vector<double> &features, int *region){
   for(int istation=0; istation<nstations; ++istation){
      double MatchedHitX = features[MatchedHitXId[istation]];
      double MatchedHitY = features[MatchedHitYId[istation]];

      if(abs(MatchedHitX)<600 && abs(MatchedHitY)<500)
	 region[istation] = 1;   
      else if(abs(MatchedHitX)<1200 && abs(MatchedHitY)<1000)
	 region[istation] = 2;   
      else if(abs(MatchedHitX)<2400 && abs(MatchedHitY)<2001)
	 region[istation] = 3;   
      else
	 region[istation] = 4;   

      if(MatchedHitX==-9999 || MatchedHitY==-9999)
	 region[istation] = 0;   
   }

   return;
}

void calc_DiffMatchedLextra(const std::vector<double> &features, double *diffX, double *diffY){
   for(int istation=0; istation<nstations; ++istation){
      double MatchedHitX = features[MatchedHitXId[istation]];
      double MatchedHitY = features[MatchedHitYId[istation]];
      double LextraX = features[LextraXId[istation]];
      double LextraY = features[LextraYId[istation]];
      diffX[istation] = MatchedHitX - LextraX;
      diffY[istation] = MatchedHitY - LextraY;
   }
}

void calc_ChiPhys(const std::vector<double> &features, double *physX, double *physY){

   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double MatchedHitDX = features[MatchedHitDXId[istation]];
      const double MatchedHitDY = features[MatchedHitDYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];

      physX[istation] = (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MatchedHitDX/MatchedHitDX;
      physY[istation] = (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MatchedHitDY/MatchedHitDY;
   }
   return ;
}

void calc_SumChiPhys(const std::vector<double> &features, double *physX, double *physY){
   *physX = 0;
   *physY = 0;
   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double MatchedHitDX = features[MatchedHitDXId[istation]];
      const double MatchedHitDY = features[MatchedHitDYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];

      *physX += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MatchedHitDX/MatchedHitDX;
      *physY += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MatchedHitDY/MatchedHitDY;
   }
   return ;
}

void calc_ChiModel(const std::vector<double> &features, double *modelX, double *modelY){

   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      modelX[istation] = (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MextraDX;
      modelY[istation] = (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MextraDY;
   }
   return;
}

void calc_SumChiModel(const std::vector<double> &features, double *modelX, double *modelY){
   *modelX = 0;
   *modelY = 0;
   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      *modelX += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MextraDX;
      *modelY += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MextraDY;
   }
   return;
}

void calc_SumChiPhysModel(const std::vector<double> &features, double *physX, double *physY, double *modelX, double *modelY){
   *physX = 0;
   *physY = 0;
   *modelX = 0;
   *modelY = 0;
   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double MatchedHitX = features[MatchedHitXId[istation]];
      const double MatchedHitY = features[MatchedHitYId[istation]];
      const double MatchedHitDX = features[MatchedHitDXId[istation]];
      const double MatchedHitDY = features[MatchedHitDYId[istation]];
      const double LextraX = features[LextraXId[istation]];
      const double LextraY = features[LextraYId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      *physX += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MatchedHitDX/MatchedHitDX;
      *physY += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MatchedHitDY/MatchedHitDY;
      *modelX += (MatchedHitX - LextraX)*(MatchedHitX - LextraX)/MextraDX;
      *modelY += (MatchedHitY - LextraY)*(MatchedHitY - LextraY)/MextraDY;
   }
   return ;
}


double calc_sqPT(const std::vector<double> &features){
   return sqrt(features[PTId] - 800)/20;
}

double calc_sqP(const std::vector<double> &features){
   return sqrt(features[PId] - 3000)/100;
}

void calc_FOIDensity(const std::vector<double> &features, double *density){

   for(int istation=0; istation<nstations; ++istation){
      const int MatchedHitType = features[MatchedHitTypeId[istation]];
      if(MatchedHitType==0) continue;
      const double FOI_N = features[FOINId[istation]];
      const double MextraDX = features[MextraDXId[istation]];
      const double MextraDY = features[MextraDYId[istation]];

      density[istation] = pow(FOI_N/sqrt(MextraDX)/sqrt(MextraDY),0.25);
   }
}

