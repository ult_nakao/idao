// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>

#include "./parser.h"
#include "./calc_var.h"
#include "ripped_evaluator/evaluator.h"

const size_t N_MY_FEATURES = 87;

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    const std::string MODEL_FILE = "Cat_20190210_nn_full.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator(MODEL_FILE);

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
    
      std::vector<double> features(N_FEATURES);
      size_t id;
      ugly_hardcoded_parse(std::cin, &id, &features);
      
      std::vector<double> my_features(N_MY_FEATURES);
      int tmp[4];
      double tmp0[4], tmp1[4], tmp2[4], tmp3[4], tmp4[4], tmp5[4];
      double sum_physX, sum_physY, sum_modelX, sum_modelY;
      double ASD = calc_Features(features,tmp,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5,&sum_physX,&sum_physY,&sum_modelX,&sum_modelY);

      my_features[28] = ASD;
      my_features[29] = sum_physX;
      my_features[30] = sum_physY;
      my_features[31] = sum_modelX;
      my_features[32] = sum_modelY;

      for(int i=0; i<4; ++i){
         my_features[0+i] = tmp0[i];//dens_FOI_N
         my_features[4+i] = tmp1[i];//MatchedHitR
         my_features[8+i] = tmp[i];//region
         my_features[12+i*4] = pow(tmp2[i],0.4);//sq_chi_physX
         my_features[13+i*4] = pow(tmp4[i],0.4);//sq_chi_modelX
         my_features[14+i*4] = pow(tmp3[i],0.4);//sq_chi_physY
         my_features[15+i*4] = pow(tmp5[i],0.4);//sq_chi_modelY
         my_features[33+i] = features[MatchedHitTypeId[i]];//MatchedHitType
         my_features[37+i] = features[MatchedHitXId[i]];//MatchedHitX
         my_features[41+i] = features[MatchedHitYId[i]];//MatchedHitY
         my_features[45+i] = features[MatchedHitZId[i]];//MatchedHitZ
         my_features[49+i] = features[MatchedHitTId[i]];//MatchedHitT
         my_features[53+i] = features[MatchedHitDXId[i]];//MatchedHitDX
         my_features[57+i] = features[MatchedHitDYId[i]];//MatchedHitDY
         my_features[61+i] = features[MatchedHitDZId[i]];//MatchedHitDZ
         my_features[65+i] = features[MatchedHitDTId[i]];//MatchedHitDT
         my_features[69+i] = 1./sqrt(features[MextraDXId[i]]);//MextraDX
         my_features[73+i] = 1./sqrt(features[MextraDYId[i]]);//MextraDY
         my_features[81+i] = features[FOINId[i]];//FOI_N
      }
      my_features[77] = calc_sqPT(features);//sq_PT
      my_features[78] = calc_sqP(features);//sq_P
      my_features[79] = calc_PCat(features);//PCat
      my_features[80] = calc_PRatio(features);//PRatio
      my_features[85] = features[NSharedId];//NShared
      my_features[86] = features[ndofId];//ndof

      const double prediction = \
            evaluator.Apply(my_features, NCatboostStandalone::EPredictionType::RawValue);
	    
      std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
