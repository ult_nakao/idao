#include "./parser.h"

inline bool is_number(const char pen) {
    if(pen == '.') return true;
    if(pen == '-') return true;
    bool flag = false;
    char tmp[8] = {pen};
    for(int i=0; i<10; ++i){
       char digit[8];
       sprintf(digit, "%d", i);
       if(!strcmp(tmp,digit)){
	  flag = true;
	  break;
       }
    }
    return flag;
}

inline bool not_number(const char pen) {
    return !isdigit(pen) && !(pen == '.') && !(pen == '-');
}

void skip_to_number_pointer(const char *& pen) {
    while ((*pen) && not_number(*pen)) ++pen;
}

inline double square(const double x) {
    return x*x;
}

// https://stackoverflow.com/questions/5678932/fastest-way-to-read-numerical-values-from-text-file-in-c-double-in-this-case
template<class T>
T rip_uint_pointer(const char *&pen, T val = 0) {
    // Will return val if *pen is not a digit
    // WARNING: no overflow checks
    for (char c; (c = *pen ^ '0') <= 9; ++pen)
        val = val * 10 + c;
    return val;
}

template<class T>
T rip_double_pointer(const char *&pen) {
    static double const exp_lookup[]
        = {1, 0.1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10,
           1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16, 1e-17};
    T sign = 1.;
    if (*pen == '-') {
        ++pen;
        sign = -1.;
    }
    uint64_t val = rip_uint_pointer<uint64_t>(pen);
    unsigned int neg_exp = 0;
    if (*pen == '.') {
        const char* const fracs = ++pen;
        val = rip_uint_pointer(pen, val);
        neg_exp  = pen - fracs;
    }
    return std::copysign(val*exp_lookup[neg_exp], sign);
}

// Warning: this is not a general-puropse parser, you have
// std::istream for that. As a rule, in the interest of speed, it
// doesn't check for input correctness and will have undefined
// behavior at incorrect input
class BufferedStream {
 public:
    explicit BufferedStream(std::istream& stream);
    // Discards data from the stream until ecountering a digit, "." or "-"
    void skip_to_number();
    // Reads a double from the stream, starting from the current character
    // and has undefined behaviour if there is no number at the
    // current position
    template<class T> T rip_double() {return rip_double_pointer<T>(pen);}
    // Reads an unsigned integer from stream, starting from the
    // current character and has undefined behaviour if there is no
    // number at the current position
    template<class T> T rip_uint() {return rip_uint_pointer<T>(pen);}
    // Reads a vector of doubles of the given size from the stream,
    // skipping everything as needed
    template<class T>
    std::vector<T> fill_vector_double(const size_t size);
    // Reads a vector of unsigned ints of the given size from the stream,
    // skipping as needed. In principle, should be templated from
    // fill_vector_double, but if constexpr is C++17 :(
    template<class T>
    std::vector<T> fill_vector_uint(const size_t size);
    // Reads count doubleing point numbers and stores them into the
    // container pointed to by the iterator
    template<class IteratorType>
    void fill_iterator_double(const IteratorType& iterator, const size_t count);
    // Discards data from the stream until encountering the delimiter
    void skip_to_char(const char delimiter);
    // Discrads data from the stream until twice encountering the delimiter
    void skip_record(const char delimiter);

 private:
    void next_line();
    // Buffer size is measured to fit the longest line in the test dataset
    // but the code doesn't rely on it
    static const size_t BUFFER_SIZE = 1016;
    char buffer[BUFFER_SIZE];
    std::istream& stream;
    const char* pen;
};

void BufferedStream::next_line() {
    stream.getline(buffer, BUFFER_SIZE);
    if(stream.fail()) {
       stream.clear(stream.rdstate() & ~std::ios_base::failbit);
    }
    pen = buffer;
}

BufferedStream::BufferedStream(std::istream& stream): stream(stream) {
    next_line();
}

void BufferedStream::skip_to_number() {
    skip_to_number_pointer(pen);
    while ((*pen) == 0) {
        next_line();
        skip_to_number_pointer(pen);
        // The skip stops either at 0-byte or
        // a number part
    }
}

template<class T>
std::vector<T> BufferedStream::fill_vector_double(const size_t size) {
    std::vector<T> result(size);
    fill_iterator_double<std::vector<double>::iterator>(result.begin(), size);
    return result;
}

template<class T>
std::vector<T> BufferedStream::fill_vector_uint(const size_t size) {
    std::vector<T> result(size);
    for (auto& value : result) {
        skip_to_number();
        value = rip_uint<T>();
    }
    return result;
}

void BufferedStream::skip_to_char(const char delimiter) {
    while ((*pen) != delimiter) {
        while ((*pen) && (*(++pen)) != delimiter) { /*std::cout << *pen << std::endl;*/}
        if (!(*pen)) next_line();
    }
}

void BufferedStream::skip_record(const char delimiter) {
    skip_to_char(delimiter);
    ++pen;
    skip_to_char(delimiter);
}


template<class IteratorType>
void BufferedStream::fill_iterator_double(const IteratorType& iterator, const size_t count) {
    for (IteratorType value = iterator; value != iterator + count; ++value) {
        skip_to_number();
        *value = rip_double<typename std::iterator_traits<IteratorType>::value_type>();
    }
}

void ugly_hardcoded_parse(std::istream& stream, size_t* id, std::vector<double>* result) {
    BufferedStream buffered_stream(stream);
    *id = buffered_stream.rip_uint<size_t>();
    buffered_stream.fill_iterator_double<std::vector<double>::iterator>(
        result->begin(), N_RAW_FEATURES - N_RAW_FEATURES_TAIL);

    // No need to skip, fill_vector takes care of it
    const size_t FOI_hits_N = (*result)[FOI_HITS_N_INDEX];
    const std::vector<double> FOI_hits_X = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    const std::vector<double> FOI_hits_Y = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    const std::vector<double> FOI_hits_Z = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    const std::vector<double> FOI_hits_DX = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    const std::vector<double> FOI_hits_DY = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    buffered_stream.skip_record(DELIMITER);
    const std::vector<double> FOI_hits_T = buffered_stream.fill_vector_double<double>(FOI_hits_N);
    buffered_stream.skip_record(DELIMITER);
    const std::vector<size_t> FOI_hits_S = \
        buffered_stream.fill_vector_uint<size_t>(FOI_hits_N);

    //calculate StdDiv per station
    //calculate FOI_hits_N per station
    for(size_t istation=0; istation<N_STATIONS; ++istation){
	 int nhits = 0;
	 double xmean = 0;
	 double ymean = 0;
	 for(size_t ihit=0; ihit<FOI_hits_N; ++ihit){
	    if(istation==FOI_hits_S[ihit]){
	       xmean += FOI_hits_X[ihit];
	       ymean += FOI_hits_Y[ihit];
	       ++nhits;
	    }
	 }
	 xmean /= nhits>0 ? nhits : 1;
	 ymean /= nhits>0 ? nhits : 1;
	 double xsigma = 0;
	 double ysigma = 0;
	 for(size_t ihit=0; ihit<FOI_hits_N; ++ihit){
	    if(istation==FOI_hits_S[ihit]){
	       xsigma += (xmean - FOI_hits_X[ihit])*(xmean - FOI_hits_X[ihit]);
	       ysigma += (ymean - FOI_hits_Y[ihit])*(ymean - FOI_hits_Y[ihit]);
	    }
	 }
	 //FOI_hits_N[0-3]
         (*result)[FOI_FEATURES_START + istation] = nhits;
	 //sigmax[0-3]
         (*result)[FOI_FEATURES_START + N_STATIONS + istation] = nhits>0 ? sqrt(xsigma / nhits) : -1;
	 //sigmay[0-3]
         (*result)[FOI_FEATURES_START + N_STATIONS*2 + istation] = nhits>0 ? sqrt(ysigma / nhits) : -1;
    }

    //calculate ASD of all hits
    /*
    double ASD = 0;
    for(size_t istation=0; istation<N_STATIONS; ++istation){
	 for(size_t ihit=0; ihit<FOI_hits_N; ++ihit){
	    if(istation==FOI_hits_S[ihit]){
	       double xdif = (*result)[LEXTRA_X_INDEX + istation] - FOI_hits_X[ihit];
	       double ydif = (*result)[LEXTRA_Y_INDEX + istation] - FOI_hits_Y[ihit];
	       ASD += xdif*xdif/(*result)[MATCHED_HIT_DX_INDEX + istation]/sqrt((*result)[MEXTRA_X_INDEX + istation]);
	       ASD += ydif*ydif/(*result)[MATCHED_HIT_DY_INDEX + istation]/sqrt((*result)[MEXTRA_Y_INDEX + istation]);
	    }
	 }   
    }
    (*result)[FOI_FEATURES_START] = ASD>0 ? N_STATIONS/ASD : 0;
    */

    /*
    std::array<size_t, N_STATIONS> closest_hit_per_station;
    std::array<double, N_STATIONS> closest_hit_distance;
    closest_hit_distance.fill(std::numeric_limits<double>::infinity());
    for (size_t hit_index = 0; hit_index < FOI_hits_N; ++hit_index) {
        const size_t this_station = FOI_hits_S[hit_index];
        const double distance_x_2 = square(FOI_hits_X[hit_index] -
                                          (*result)[LEXTRA_X_INDEX + this_station]);
        const double distance_y_2 = square(FOI_hits_Y[hit_index] -
                                          (*result)[LEXTRA_Y_INDEX + this_station]);
        const double distance_2 = distance_x_2 + distance_y_2;
        if (distance_2 < closest_hit_distance[this_station]) {
            closest_hit_distance[this_station] = distance_2;
            closest_hit_per_station[this_station] = hit_index;
            (*result)[FOI_FEATURES_START + this_station] = distance_x_2;
            (*result)[FOI_FEATURES_START + N_STATIONS + this_station] = distance_y_2;
        }
    }*/
    /* [closest_x_per_station, closest_y_per_station, closest_T_per_station,
       closest_z_per_station, closest_dx_per_station, closest_dy_per_station]) */
    /*
    for (size_t station = 0; station < N_STATIONS; ++station) {
        if (std::isinf(closest_hit_distance[station])) {
            for (size_t feature_index = 0;
                 feature_index < FOI_FEATURES_PER_STATION;
                 ++feature_index) {
                (*result)[FOI_FEATURES_START + feature_index * N_STATIONS + station] = EMPTY_FILLER;
            }
        } else {
            // x, y have already been filled by the closest hit search
            (*result)[FOI_FEATURES_START + 2 * N_STATIONS + station] =  \
                FOI_hits_T[closest_hit_per_station[station]];
            (*result)[FOI_FEATURES_START + 3 * N_STATIONS + station] =  \
                FOI_hits_Z[closest_hit_per_station[station]];
            (*result)[FOI_FEATURES_START + 4 * N_STATIONS + station] =  \
                FOI_hits_DX[closest_hit_per_station[station]];
            (*result)[FOI_FEATURES_START + 5 * N_STATIONS + station] =  \
                FOI_hits_DY[closest_hit_per_station[station]];
        }
    }*/
    buffered_stream.fill_iterator_double<std::vector<double>::iterator>(
        result->begin() + N_RAW_FEATURES - N_RAW_FEATURES_TAIL, N_RAW_FEATURES_TAIL);
}

