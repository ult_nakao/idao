// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>

#include "./parser.h"
#include "./calc_var.h"
#include "ripped_evaluator/evaluator.h"

const size_t N_MY_FEATURES = 45;

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    const std::string MODEL_FILE = "Cat_20190209_1.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator(MODEL_FILE);

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
    
      std::vector<double> features(N_FEATURES);
      size_t id;
      ugly_hardcoded_parse(std::cin, &id, &features);
      
      std::vector<double> my_features(N_MY_FEATURES);
      int tmp[4];
      double tmp0[4], tmp1[4], tmp2[4], tmp3[4];
      calc_MatchedHitRdiff(features,tmp0);
      my_features[0] = tmp0[0];//MatchedHitR_01
      calc_MatchedHitR(features,tmp0);
      my_features[1] = tmp0[1];//MatchedHitR_1
      my_features[2] = tmp0[0];//MatchedHitR_0
      calc_region(features,tmp);
      for(int i=0; i<4; ++i)
	 my_features[3+i] = tmp[i];//region
      my_features[7] = features[FOIHitsNId];//FOI_hits_N
      my_features[8] = features[PTId];//PT
      my_features[9] = features[PId];//P
      my_features[10] = calc_PCat(features);//PCat
      my_features[11] = calc_PRatio(features);//PRatio
      calc_DiffMatchedLextra(features,tmp0,tmp1);
      for(int i=0; i<4; ++i){
         my_features[12+i] = tmp0[i];//DiffMatchedLextra_X
         my_features[16+i] = tmp1[i];//DiffMatchedLextra_Y
      }
      my_features[20] = calc_AverageSquaredDistanceInv(features);//invASD
      calc_ChiPhys(features,tmp0,tmp1);
      calc_ChiModel(features,tmp2,tmp3);
      for(int i=0; i<4; ++i){
         my_features[21+i*4] = tmp0[i];//chi_phys_X
         my_features[22+i*4] = tmp2[i];//chi_model_X
         my_features[23+i*4] = tmp1[i];//chi_phys_Y
         my_features[24+i*4] = tmp3[i];//chi_model_Y
         my_features[37+i] = features[FOINId[i]];//FOI_N
      }
      my_features[41] = check_MatchedHitPattern1(features);
      my_features[42] = check_MatchedHitPattern2(features);
      my_features[43] = calc_sqPT(features);//sq_PT
      my_features[44] = calc_sqP(features);//sq_P


      const double prediction = \
            evaluator.Apply(my_features, NCatboostStandalone::EPredictionType::RawValue);
	    
      std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
