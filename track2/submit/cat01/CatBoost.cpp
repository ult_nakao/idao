// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>

#include "./parser.h"
#include "./calc_var.h"
#include "ripped_evaluator/evaluator.h"

const size_t N_MY_FEATURES0 = 36;
const size_t N_MY_FEATURES1 = 45;

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    const std::string MODEL_FILE0 = "Cat_20190209_0.cbm";
    const std::string MODEL_FILE1 = "Cat_20190209_1.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator0(MODEL_FILE0);
    NCatboostStandalone::TOwningEvaluator evaluator1(MODEL_FILE1);

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
    
      std::vector<double> features(N_FEATURES);
      size_t id;
      ugly_hardcoded_parse(std::cin, &id, &features);
      
      std::vector<double> my_features0(N_MY_FEATURES0);
      std::vector<double> my_features1(N_MY_FEATURES1);
      int region[4];
      double R[4], Rdiff[3], tmp0[4], tmp1[4], tmp2[4], tmp3[4], tmp4[4], tmp5[4];
      my_features0[0] = features[FOIHitsNId];//FOI_hits_N 
      my_features0[1] = features[PTId];//PT
      my_features0[2] = features[PId];//P
      int PCat = calc_PCat(features);
      double PRatio = calc_PRatio(features);//PRatio
      my_features0[3] = PCat;//PCat
      my_features0[4] = PRatio;//PRatio

      double invASD = calc_Features(features,region,R,Rdiff,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5);
      my_features1[0] = tmp1[0];//MatchedHitR_01
      my_features1[1] = tmp0[1];//MatchedHitR_1
      my_features1[2] = tmp0[0];//MatchedHitR_0
      my_features1[7] = features[FOIHitsNId];//FOI_hits_N
      my_features1[8] = features[PTId];//PT
      my_features1[9] = features[PId];//P
      my_features1[10] = PCat;//PCat
      my_features1[11] = PRatio;//PRatio
      my_features0[13] = invASD;
      my_features1[20] = invASD;
      for(int i=0; i<4; ++i){
	 my_features1[3+i] = region[i];//region
         my_features0[5+i] = tmp0[i];//DiffMatchedLextra_X
         my_features0[9+i] = tmp1[i];//DiffMatchedLextra_Y
         my_features1[12+i] = tmp0[i];//DiffMatchedLextra_X
         my_features1[16+i] = tmp1[i];//DiffMatchedLextra_Y
         my_features0[14+i*4] = tmp2[i];//chi_phys_X
         my_features0[15+i*4] = tmp4[i];//chi_model_X
         my_features0[16+i*4] = tmp3[i];//chi_phys_Y
         my_features0[17+i*4] = tmp5[i];//chi_model_Y
         my_features0[30+i] = features[FOINId[i]];//FOI_N
         my_features1[21+i*4] = tmp2[i];//chi_phys_X
         my_features1[22+i*4] = tmp4[i];//chi_model_X
         my_features1[23+i*4] = tmp3[i];//chi_phys_Y
         my_features1[24+i*4] = tmp5[i];//chi_model_Y
         my_features1[37+i] = features[FOINId[i]];//FOI_N
      }
      my_features1[41] = check_MatchedHitPattern1(features);
      my_features1[42] = check_MatchedHitPattern2(features);
      double sqPT = calc_sqPT(features);//sq_PT
      double sqP = calc_sqP(features);//sq_PT
      my_features0[34] = sqPT;//sq_PT
      my_features0[35] = sqP;//sq_P
      my_features1[43] = sqPT;//sq_PT
      my_features1[44] = sqP;//sq_P


      const double prediction0 = \
            evaluator0.Apply(my_features0, NCatboostStandalone::EPredictionType::RawValue);

      const double prediction1 = \
            evaluator1.Apply(my_features1, NCatboostStandalone::EPredictionType::RawValue);
	    
      std::cout << id << DELIMITER << (prediction0+prediction1)/2  << '\n';
    }
    return 0;
}
