// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>

#include "./parser.h"
#include "./calc_var.h"
#include "ripped_evaluator/evaluator.h"

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
        std::vector<double> features(N_FEATURES);
        size_t id;
        ugly_hardcoded_parse(std::cin, &id, &features);
        double prediction = calc_AverageSquaredDistanceInv(features);

	std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
