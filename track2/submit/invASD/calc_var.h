//#pragma once
#include <iostream>
#include <cmath>
#include <vector>
//#include <parser.h> cannot include

//must be the same order as the original csv input
const int nstations = 4;
const int nclId[] = {0, 1, 2, 3};
const int avg_csId[] = {4, 5, 6, 7};
const int ndofId = 8;
const int MatchedHitTypeId[] = {9, 10, 11, 12};
const int MatchedHitXId[]  = {13, 14, 15, 16};
const int MatchedHitYId[]  = {17, 18, 19, 20};
const int MatchedHitZId[]  = {21, 22, 23, 24};
const int MatchedHitDXId[] = {25, 26, 27, 28};
const int MatchedHitDYId[] = {29, 30, 31, 32};
const int MatchedHitDZId[] = {33, 34, 35, 36};
const int MatchedHitTId[]  = {37, 38, 39, 40};
const int MatchedHitDTId[] = {41, 42, 43, 44};
const int LextraXId[]      = {45, 46, 47, 48};
const int LextraYId[]      = {49, 50, 51, 52};
const int NSharedId        = 53;
const int MextraDXId[]      = {54, 55, 56, 57};
const int MextraDYId[]      = {58, 59, 60, 61};
const int FOIHitsNId       = 62;
const int PTId             = 63;
const int PId              = 64;
const int FOINId[]         = {65, 66, 67, 68};

double calc_AverageSquaredDistanceInv(const std::vector<double> &features);
double calc_AverageSquaredDistance(const std::vector<double> &features);
int check_MatchedHitPattern1(const std::vector<double> &features);
int check_MatchedHitPattern2(const std::vector<double> &features);
void calc_sigma_x(const std::vector<double> &featrues, double *results);
int calc_PCat(const std::vector<double> &featrues);
double calc_PRatio(const std::vector<double> &featrues);
void calc_MatchedHitR(const std::vector<double> &features, double *R);
void calc_MatchedHitRdiff(const std::vector<double> &features, double *R);
void calc_region(const std::vector<double> &features, int *region);
void calc_DiffMatchedLextra(const std::vector<double> &features, double *diffX, double *diffY);
void calc_ChiPhys(const std::vector<double> &features, double *physX, double *physY);
void calc_SumChiPhys(const std::vector<double> &features, double *physX, double *physY);
void calc_ChiModel(const std::vector<double> &features, double *modelX, double *modelY);
void calc_SumChiModel(const std::vector<double> &features, double *modelX, double *modelY);
void calc_ChiPhysModel(const std::vector<double> &features, double *physX, double *physY, double *modelX, double *modelY);
void calc_SumChiPhysModel(const std::vector<double> &features, double *physX, double *physY, double *modelX, double *modelY);
void calc_Features(const std::vector<double> &features, int *region, double *density, double *R, double *physX, double *physY, double *modelX, double *modelY, double *sum_physX, double *sum_physY, double *sum_modelX, double *sum_modelY);
double calc_sqPT(const std::vector<double> &features);
double calc_sqP(const std::vector<double> &features);
void calc_FOIDensity(const std::vector<double> &features, double *density);
