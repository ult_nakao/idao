import sys
import csv
import numpy as np
sys.path.append('../yandex/')
import scoring
import utils

DATA_PATH = "../your_data_dir/"
READ_COLUMNS = ['weight', 'label']

def main():
    print('reading data')
    train = utils.load_data_csv_part(DATA_PATH, READ_COLUMNS, 'train_part_2')
    #train, test = utils.load_data_csv(DATA_PATH, utils.SIMPLE_FEATURE_COLUMNS)
    print('finish read data')
    predict_list = []
    with open('submmision_train2.csv', 'r') as f:
    #with open('submmision.csv', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            predict_list.append(float(row[1]))
    predictions_score_predict = np.array(predict_list)
    weights_score_predict = train['weight'].values
    labels_score_predict = train['label'].values
    score = scoring.rejection90(labels_score_predict, predictions_score_predict, weights_score_predict)
    print('your score : ' + str(score))

if __name__ == "__main__":
    main()

