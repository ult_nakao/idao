// Copyright 2019, Nikita Kazeev, Higher School of Economics
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <vector>
#include <limits>
#include <string>
#include <time.h>
//#include <cctype>
//#include <algorithm>

#include "./parser.h"
#include "ripped_evaluator/evaluator.h"

int main() {
    clock_t sttime = clock();
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    const std::string MODEL_FILE = "track_2_model.cbm";
    NCatboostStandalone::TOwningEvaluator evaluator(MODEL_FILE);
    std::ifstream fin;
    fin.open("../../your_data_dir/test_public_v2.csv");
    std::ofstream fout;
    fout.open("./submmision.csv");
    std::string line;
    bool header = true;
    int iline = 0;
    while(std::getline(fin, line)){
      if(iline++==10) break;
      if(header) {
	 header = false;
	 continue;
      }
      std::vector<float> features(N_FEATURES);
      size_t id;
      std::istringstream stream1(line);
      std::string input1; 
      std::cout << line << std::endl;
      ugly_hardcoded_parse(stream1, &id, &features);
      /*
      int ifeature = 0;
      bool first = true;
      while(std::getline(stream1,input1,',')){
	 if(ifeature>=(int)N_FEATURES) break;
	 if(first) {
	    id = std::stoi(input1);
	    first = false;
	    std::cout << "id = " << id << std::endl;
	    continue;
	 }
	 //std::cout<< input1 << std::endl;
	 if(input1.find("[")==std::string::npos){
	    features[ifeature++] = std::stod(input1);
	    //std::cout << features[ifeature-1] << ' ' << ifeature-1 << ' ' << N_FEATURES << std::endl;
	 }else{
	    continue;
	    input1.erase(std::find(input1.begin(),input1.end(),'['));
	    input1.erase(std::find(input1.begin(),input1.end(),']'));

	    std::istringstream stream2(input1);
	    std::string input2; 
	    while(std::getline(stream2,input2,' ')){
	       if(ifeature>=(int)N_FEATURES) break;
	       bool IsDigit = false;
	       for(int i=0; i<10; ++i){
		  if(input2.find(std::to_string(i))!=std::string::npos){
		     IsDigit = true;
		     break;
		  }
	       }
	       if(IsDigit){
		  features[ifeature++] = std::stod(input2);
		  std::cout << features[ifeature-1] << ' ' << ifeature-1 << ' ' << N_FEATURES << std::endl;
	       }
	    }
	 }
      }
      */
      const float prediction = \
            evaluator.Apply(features, NCatboostStandalone::EPredictionType::RawValue);
      fout << id << DELIMITER << prediction  << '\n';
      
    }
    clock_t entime = clock();
    std::cout << "calculation time : " << double(entime - sttime) / CLOCKS_PER_SEC << " (s)" << std::endl;
    return 1;
    // Skip header
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "start prediction" << std::endl;
    std::cout << std::setprecision(std::numeric_limits<float>::max_digits10);
    std::cout << "id,prediction\n";
    while (std::cin.good() && std::cin.peek() != EOF) {//check input and finish if EndOfFile
	std::vector<float> features(N_FEATURES);
        size_t id;
        ugly_hardcoded_parse(std::cin, &id, &features);
        const float prediction = \
            evaluator.Apply(features, NCatboostStandalone::EPredictionType::RawValue);
        std::cout << id << DELIMITER << prediction  << '\n';
    }
    return 0;
}
